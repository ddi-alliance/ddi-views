.. _Glossary:

********
Glossary
********

.. glossary::

   Abstract class 
      In programming languages, an abstract type is a type in a nominative type system which cannot be instantiated directly. 
      Its only purpose is for other classes to extend (see http://en.wikipedia.org/wiki/Abstract_type).

   Binding 
      Data binding is a way to un/serialize objects across programs, languages, and platforms. 
      The structure and the data remain consistent and coherent throughout the journey, and no custom formats or parsing 
      are required (see http://en.wikipedia.org/wiki/XML_data_binding).

   Canonical
      Conforming to a general rule or acceptable procedure (see http://www.merriam-webster.com/dictionary/canonical).

   Codebook
      A codebook is a type of document used for gathering and storing codes. Originally codebooks were often literally books, but today codebook is a byword for 
      the complete record of a series of codes, regardless of physical format. See Wikipedia -- http://en.wikipedia.org/wiki/Codebook. 
      DDI Codebook is the development line of the DDI specification that reflects the content of codebooks.

   Complex Data Types
      An extended data type is a user-defined definition of a primitive data type. The following primitive data types can be 
      extended: boolean, integer, real, string, date and container (see http://www.axaptapedia.com/Extended_Data_Types).

   Content Capture
      Content capture refers to the process of harvesting content from other sources. For DDI 4 development, content is being captured using the Drupal content management
      system to permit machine-processing.

   Content Modeler
      One of the group of people determining the requirements for a Functional View and then identifying the set of objects needed to meet those requirements. 
      In this process they may also need to describe new objects.

   Data Modeler
      These people work with Content Modelers to insure that new objects are consistent with the objects accepted into the approved model. 
      Data Modelers also arrange objects into the final namespace structure of the published model.

   DDI
      The Data Documentation Initiative (DDI) is an effort to create an international standard for describing data from the social, behavioral, and economic sciences. 
      Versions upto DDi-Lifecycl are expressed solely in XML in, the DDI metadata specification now supports the entire research data life cycle. 
      DDI metadata accompanies and enables data conceptualization, collection, processing, distribution, discovery, analysis, repurposing, and archiving.

   Drupal
      Drupal is a free and open-source content management framework written in PHP and distributed under the GNU General Public License. 
      It is also used for knowledge management and business collaboration (see http://en.wikipedia.org/wiki/Drupal).
      
   Enterprise Architect
      Enterprise Architect is a visual modeling and design tool based on the OMG UML. The platform supports: the design and construction of software systems; 
      modeling business processes; and modeling industry based domains. It is used by businesses and organizations to not only model the architecture of their systems, 
      but to process the implementation of these models across the full application development life-cycle (see http://en.wikipedia.org/wiki/Enterprise_Architect_(Visual_Modeling_Platform).

   Extension
      Extension is the inheritance of one object’s properties and relationships from another object. It also has a semantic relationship – 
      an extending object provides a specialized use of the extended object. Extensions are used within the DDI-published packages to provide relationships between
      objects as they increase in complexity to meet increasingly complex functionality. Thus, a “simple” version of a questionnaire object might be extended into
      a more complex object, describing a more complex questionnaire.
      
   Framework
      The basic structure of something; a set of ideas or facts that provide support for something; a supporting structure; 
      a structural frame (see http://www.merriam-webster.com/dictionary/framework).
      
   Functional View
      In DDI 4, a functional view identifies a set of objects that are needed to perform a specific task. 
      It primarily consists of a set of references to specific versions of objects. 
      Functional Views are the method used to restrict the portions of the model that are used, and as such they function very much like DDI profiles in DDI 3.*.

   GLBPM
     The Generic Longitudinal Business Process Model  is a reference model of the process of longitudinal and repeat cross-sectional data collection, 
     describing the activities undertaken and mapping these to their typical inputs and outputs, which would then be described using DDI Lifecycle.
     See http://www.ddialliance.org/system/files/GenericLongitudinalBusinessProcessModel.pdf .
     
   GSBPM
     The Generic Statistical Business Process Model describes and defines the set of business processes needed to produce official statistics. 
     It provides a standard framework and harmonised terminology to help statistical organisations to modernise their statistical production processes, as well as to share methods and components. 
     The GSBPM can also be used for integrating data and metadata standards, as a template for process documentation, for harmonizing statistical computing infrastructures, and to provide a framework for process quality assessment and improvement
     See http://www1.unece.org/stat/platform/display/GSBPM/Generic+Statistical+Business+Process+Model.
   
   GSIM
     The Generic Statistical Information Model provides the information object framework supporting all statistical production processes such as those described in the 
     Generic Statistical Business Process Model (GSBPM), giving the information objects agreed names, defining them, specifying their essential properties, 
     and indicating their relationships with other information objects. It does not, however, make assumptions about the standards or technologies used to implement the model.
     See http://www1.unece.org/stat/platform/display/gsim/Generic+Statistical+Information+Model 
    
   Identification
      In the DDI specifications, each object is uniquely identified.

   Instantiate 
      To create an object of a specific class (see http://en.wiktionary.org/wiki/instantiate).

   Library
      The Object Library for DDI 4 encompasses the entire DDI 4.0 model, but without any specific schemas or vocabularies for Functional Views. 
      Objects contain primitives and extended primitives and are the building blocks used to construct the Functional Views. 
      Objects are organized into packages in the Library.

   Lifecycle
      The research data lifecycle is a set of processes that begins at study inception and progresses through data collection, data publication, data archiving, 
      and beyond. DDI created a lifecycle model in 2004 to describe this flow (see http://www.ddialliance.org/system/files/Concept-Model-WD.pdf).
      
   Management package
      DDI View packages containing library constructs – primitives, extended primitives, objects, and functional views -- which are organized thematically.
 
   Metadata
      Data about data.

   Modeling
      The representation, often mathematical, of a process, concept, or operation of a system, often implemented by a computer program. 
      For DDI Views development, we are using the Unified Modeling Language (UML) to model the specification.

   Namespace
      A grouping of objects that allows for objects with the same name to be differentiated. The full name of an object is a combination of its namespace and its 
      name within the namespace.

   Object
      In the DDI4 model objects are representations of real entities. Objects have properties and relationships. An example might include an
      “Author” object that might have a Name attribute and an affiliation property linking to an “Organization” object.

   Ontology
      A formal representation of knowledge (see http://en.wikipedia.org/wiki/Ontology_%28information_science%29).

   OWL
      Web Ontology Language,“ a semantic mark-up language for publishing and sharing ontologies on the World Wide Web” (see http://www.w3.org/TR/owl-ref/).
      
   Package /UML Package 
      A grouping of objects in the Library (see namespace).
      
   Platform
      A pre-existing environment in which to represent data and metadata (see for example http://en.wikipedia.org/wiki/Computing_platform).
      
   Primitive
      A basic type is a data type provided by a programming language as a basic building block. Most languages allow more 
      complicated composite types to be recursively constructed starting from basic types.

   rdf
      The Resource Description Framework (RDF) is a family of 
      World Wide Web Consortium (W3C) specifications[1] originally 
      designed as a metadata data model. It has come to be used as a 
      general method for conceptual description or modeling of 
      information that is implemented in web resources, using a 
      variety of syntax notations and data serialization formats. 
      It is also used in knowledge management applications.

   xml
      Extensible Markup Language (XML) is a markup language that defines a 
      set of rules for encoding documents in a format which is both 
      human-readable and machine-readable.