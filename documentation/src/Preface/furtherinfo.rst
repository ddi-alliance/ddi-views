Further Information
====================

Further information on the development of DDI-4 is available at the `DDI Moving Forward Project site <https://ddi-alliance.atlassian.net/wiki/pages/viewpage.action?pageId=491703>`_.
