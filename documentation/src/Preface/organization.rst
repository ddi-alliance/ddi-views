Organization of this Document
==============================

The introduction provides a high level view of the structure of the DDI-4 model, its
architecture and the way in which the individual classes are organized. The introduction also provides
information on the mechanisms by which the classes are utilized, the relationships among them, how the
model can be extended and how it will be managed.

Each Library Package has a diagram showing the relationships between the classes within that Library
Package. For each class, information is provided on whether it extends any class, and whether it is based
on an existing class in DDI 3.2, GSIM or other standards.

* A definition of the class, its properties and relationship to other classes
* Examples of the usage of the class and explanatory notes
* Whether the class is abstract
* Whether the class can be extended
* Mapping to the General Statistical Information Model (GSIM) [http://www1.unece.org/stat/platform/display/gsim/Generic+Statistical+Information+Model]
* Mapping to DDI 3.2 [http://www.ddialliance.org/Specification/DDI-Lifecycle/3.2/]
* Mapping to RDF (pending)
* Mapping to Other standards (pending)

The Views section contains the following information for each Functional View:

* Purpose
* Use Cases
* Target Audiences
* Restricted Classes
* Other documentation
* Clickable graph
