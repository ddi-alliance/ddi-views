*********************
DDI 4 Model Overview
*********************
|
Introduction
============

The intent of DDI-4 Specification Class Description is to provide information on the individual classes used in the model, their relationship to each other and their relationship to DDI Lifecycle
3.2 and other standards such as General Statistical Information Model (GSIM).

The model based DDI specification consists of two parts – a Library of classes and Functional Views of the model. The Library of classes encompasses the entire DDI-Lifecycle (MD) model. The classes in the Library are the building blocks used to construct the Functional Views. These Functional Views are in essence profiles of the full specification oriented around specific user needs.
The model is primarily being developed and surfaced to the user community at http://lion.ddialliance.org/

A Note on Terminology
During the development process, the terminology for what is now called classes (to reflect the language used in UML) was previously referred to as 'objects'

Structure of the DDI 4 Model
=================================

The model contains a Library and Functional Views. The Library is composed of Library Packages which contain other data types (primitives or complex) or classes. The Functional Views contain references to the classes used by the particular Functional View that are needed to meet the needs of the use case or business application.

**Figure 1. DDI-Views Model and its components**

.. |figure1| image:: ../images/ddi4_model_structure_overview.png

|figure1|

Library of Classes
==================

The Library of Classes encompasses the entire DDI-Views model, but without any specific schemas or vocabularies for Functional Views. The classes in the Library contain primitives and complex data types and are the building blocks used to construct the Functional Views. Classes are organized into Library Packages.

Functional Views
=================

Overview
---------
A Functional View is a subset of classes from the DDI Class Library selected to support a particular activity or use case, for example, management of a statistical classification, the content of a standard data dictionary section of a codebook, a questionnaire, a concept registry, etc. The classes are provided in the Functional View without physical restriction, that is, all properties and relationships are retained. This is done to support interoperability between Functional Views and the full DDI Class Library. 
In most functional views one or more classes may be constrained, that is, the target class of a relationship is not included in the Functional View. This generally occurs where the content touches or links into another area of the DDI Class Library that does not apply to the functional activity being described. These restrictions are documented by the Functional View. The user can choose to provide an identifier for a class external to the Functional View but should be aware that systems supporting a specific Functional View will not be able to work with this content.

Definition:
------------
A sub-set of the DDI class Library selected to address a common Use Case(s) within a Target Audience(s).

Purpose:
---------
A primary goal of a Functional View is to reduce complexity in the documentation and bindings of the view as compared to the whole standard. At the same time it is desirable to have the maximal degree of compatibility among views. Several views may be applied to a given collection of data through the data’s lifecycle. Ideally metadata accumulated through the lifecycle should move smoothly through the views without loss.  
A view should be defined with very broad use case in mind, e.g. methodology, classification, discovery. More specific use cases (e.g. those usually associated with profiles) within the broad category of a view will be addressed in the documentation, e.g. which properties to include, which ones to exclude, how classes in the view should be used, etc.

Requirements:
-------------
1. Clear definition of the purpose (Use Cases and Target Audience) for each Functional View
2. Clear definition of which classes are included and their specific use within the Functional View
3. Clear head class that allows identification of all included classes within the Functional View and all included objects in an instance of that view. This is the head class of the Functional View but there may be 1..n instances. 

Structure:
-----------
1. A Functional View will be a subset of classes from the Library. They will be included with no restrictions or extensions. 
2. All classes will retain the DDI Library namespace.
3. Documentation will be used to clarify what the properties and relationships of a class are relevant or “core” to the Use Case of the Functional View. The target class of all required relationships should be included in the “core”.
4. Any reference from an included class to a class NOT contained in a Functional View will be defined as an external reference. It is the responsibility of the user to ensure that the referenced class is available in a useable format for the particular instance (i.e. similar binding). The user should assume that someone else supporting this view will NOT be able to manage the external content. If this content is essential to the instance then the use of the specific Functional View should be reconsidered.

Content of Structure:
------------------------
1. All classes to be included in the Functional View. Note that any classes not specifically included in the Functional View but acting as the target a relationship in an included class will be considered an external reference.
2. Certain classes of Functional Views may have recommended content to ensure consistency in discovery and/or self-descriptive documentation of an instance of the Functional View. For the purpose of the Prototype a required Document Information with the minimal content of an identification for the instance has been added to the Functional View.
3. A Functional View must have a required class that will serve as a starting point for entry into an object model hierarchy identifying all the component classes that make up the Functional View

Documentation in the Definition of a Functional View:
------------------------------------------------------
1. A Functional View will contain a description of the Target Audience and Uses Cases addressed by the Functional View
2. Documentation on use of restricted classes within the context of the Functional View is provided.

Applied Use of Functional Views:
-----------------------------------
1. Common core information (as defined by the Functional View based on DDI guidelines) should be completed if the binding is to be shared, in all or part, with other systems. This includes access to the instance or its contents in external search or retrieval systems.
2. Extensions of the Functional View beyond the documented usage of the classes with the Functional View should FIRST use the existing properties and relationships within the class.
3. When there is a need to restrict a Functional View it should be done on the instance level (i.e. like a DDI-L profile).
