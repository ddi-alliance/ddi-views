.. _fields:


BusinessProcess - Properties and Relationships
**********************************************





Properties
==========

=================  =======================================  ===========
Name               Type                                     Cardinality
=================  =======================================  ===========
postCondition      BusinessProcessCondition                 0..n
preCondition       BusinessProcessCondition                 0..n
standardModelUsed  PairedExternalControlledVocabularyEntry  0..n
=================  =======================================  ===========


postCondition
#############
A condition for completing and exiting the Business Process which may include the specification of the data set resulting from the Business Process.


preCondition
############
A condition for beginning the Business Process which may include the specification of the data set required for beginning the Business Process.


standardModelUsed
#################
If a standard process model such as The Generic Statistical Business Process Model (GSBPN), the Generic Longitudinal Business Process Model (GLBPM), Open Archive Information System model (OAIS), etc. has been related to this business process, the model and step or sub-step is noted here using the Paired External Controlled Vocabulary Entry. Enter the name of the model in "term" and the step, sub-step, or specific portion of the model in "extent".

