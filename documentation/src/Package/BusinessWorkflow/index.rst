****************
BusinessWorkflow
****************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   BusinessAlgorithm/index.rst
   BusinessProcess/index.rst
   DataPipeline/index.rst



Graph
=====

.. graphviz:: /images/graph/BusinessWorkflow/BusinessWorkflow.dot