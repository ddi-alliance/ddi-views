.. _DataPipeline:


DataPipeline
************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A DataPipeline is a single traversal of the Generic Longitudinal Business Model (GLBPM) and/or the Generic Statistical Business process Model (GSBPM) in the course of a study where a study is either one off or a wave in a StudySeries.


Synonyms
========
Traversal, Data Lifecycle


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst