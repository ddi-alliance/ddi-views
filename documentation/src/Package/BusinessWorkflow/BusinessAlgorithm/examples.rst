.. _examples:


BusinessAlgorithm - Examples
****************************


There are several well-established algorithms for performing data anonymization including "local suppression", "global attribute generalization" and "k-Anonymization with suppression" which is a specific combination of the two. An anonymizing business process might implement one of these algorithms.