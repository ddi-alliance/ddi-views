.. _examples:


Act - Examples
**************


QuestionConstructType from DDI 3.x is an example of an Act. GenerationInstructionType from DDI 3..x is an example of an Act extended to include a CommandCode used to create a derivation or recode for an InstanceVariable or other similar use