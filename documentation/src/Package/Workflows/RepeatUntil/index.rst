.. _RepeatUntil:


RepeatUntil
***********


Extends
=======
:ref:`ConditionalControlStep`


Definition
==========
Iterative control structure to be repeated until a specified condition is met. After each iteration the condition is tested. If the condition is not met, the associated Workflow Sequence in contains (inherited from Conditional Control Construct) is triggered. When the condition is met, control passes back to the containing Workflow Step.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst