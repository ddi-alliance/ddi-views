.. _fields:


ComputationAction - Properties and Relationships
************************************************





Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
activityDescription  InternationalStructuredString      0..1
typeOfComputation    ExternalControlledVocabularyEntry  0..1
usesCommandCode      CommandCode                        0..1
===================  =================================  ===========


activityDescription
###################
Describes the transformation activity


typeOfComputation
#################
Allows for the specification of a computation or transformation type supporting the use of an external controlled vocabulary


usesCommandCode
###############
The command code performed as a part of the transformation or computation

