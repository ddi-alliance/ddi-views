*********
Workflows
*********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Act/index.rst
   ComputationAction/index.rst
   ConditionalControlStep/index.rst
   IfThenElse/index.rst
   Loop/index.rst
   MetadataDrivenAction/index.rst
   Parameter/index.rst
   RepeatUntil/index.rst
   RepeatWhile/index.rst
   Split/index.rst
   SplitJoin/index.rst
   TemporalRelationControlStep/index.rst
   WorkflowControlStep/index.rst
   WorkflowProcess/index.rst
   WorkflowService/index.rst
   WorkflowStep/index.rst
   WorkflowStepSequence/index.rst



Graph
=====

.. graphviz:: /images/graph/Workflows/Workflows.dot