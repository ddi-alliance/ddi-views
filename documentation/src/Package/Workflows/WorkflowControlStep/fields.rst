.. _fields:


WorkflowControlStep - Properties and Relationships
**************************************************





Relationships
=============

========  ==================  ===========
Name      Type                Cardinality
========  ==================  ===========
executes  WorkflowStep        0..n
realizes  ProcessControlStep  0..n
========  ==================  ===========


executes
########
The WorkflowSteps executed by the control mechanism




realizes
########
A realization of the specified Process Pattern class



