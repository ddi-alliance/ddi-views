.. _WorkflowControlStep:


WorkflowControlStep
*******************


Extends
=======
:ref:`WorkflowStep`


Definition
==========
A subtype of WorkflowStep which controls the ordering of Workflow Steps within a Process. Abstract base for specialized ordering conditions that have specific requirements to relay ordering information.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst