.. _fields:


IfThenElse - Properties and Relationships
*****************************************





Properties
==========

======  ============  ===========
Name    Type          Cardinality
======  ============  ===========
elseIf  ElseIfAction  0..n
======  ============  ===========


elseIf
######
The condition to check if the IfThenElse condition is false. 




Relationships
=============

============  ====================  ===========
Name          Type                  Cardinality
============  ====================  ===========
elseExecutes  WorkflowStepSequence  0..n
============  ====================  ===========


elseExecutes
############
Optional sequence of Process Steps that are triggered for execution when the condition is false.



