.. _AlgorithmOverview:


AlgorithmOverview
*****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
High level, descriptive, human informative, algorithm statement used to describe the overall methodology.


Synonyms
========



Explanatory notes
=================
This would most commonly be used in a Codebook along with a MethodologyOverview and a DesignOverview.The underlying properties of the algorithm or method rather than the specifics of any particular implementation. In short a description of the method in its simplest and most general representation.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst