.. _StatisticalClassification:


StatisticalClassification
*************************


Extends
=======
:ref:`EnumerationDomain`


Definition
==========
A Statistical Classification is a set of Categories represented by Classification Items which may be assigned to one or more variables registered in statistical surveys or administrative files, and used in the production and dissemination of statistics. The Categories at each Level of the classification structure must be mutually exclusive and jointly exhaustive of all objects/units in the population of interest. (Source: GSIM StatisticalClassification)


Synonyms
========



Explanatory notes
=================
A Classification Item represents a Category at a certain Level within a Statistical Classification. The Categories are defined with reference to one or more characteristics of a particular universe of units of observation. A Statistical Classification may have a flat, linear structure or may be hierarchically structured, such that all Categories at lower Levels are sub-Categories of Categories at the next Level up. (Source: GSIM StatisticalClassification)


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst