.. _ClassificationSeries:


ClassificationSeries
********************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A Classification Series is an ensemble of one or more Statistical Classifications, based on the same concept, as documented by the definingConcept relationship, and related to each other as versions or updates. Typically, these Statistical Classifications have the same name.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst