.. _fields:


EnumerationDomain - Properties and Relationships
************************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
isOrdered  Boolean                        0..1
name       ObjectName                     0..n
purpose    InternationalStructuredString  0..1
type       CollectionType                 0..1
=========  =============================  ===========


isOrdered
#########
Allows for the identification of the member and optionally provides an index for the member within an ordered array


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided 


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates




Relationships
=============

==================  ==============  ===========
Name                Type            Cardinality
==================  ==============  ===========
definingConcept     Concept         0..n
references          CategorySet     0..n
usesLevelStructure  LevelStructure  0..n
==================  ==============  ===========


definingConcept
###############
The conceptual basis for the collection of members.




references
##########
CategorySet associated with the enumeration




usesLevelStructure
##################
Has meaningful level to which members belong.



