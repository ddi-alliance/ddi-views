.. _EnumerationDomain:


EnumerationDomain
*****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
An abstract base to allow all codifications (CodeList, StatisticalClassification, etc.) to be used as enumerated value representations.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst