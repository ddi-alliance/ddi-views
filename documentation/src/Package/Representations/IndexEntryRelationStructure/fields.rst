.. _fields:


IndexEntryRelationStructure - Properties and Relationships
**********************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasMemberRelation         IndexEntryRelation                 0..n
hasRelationSpecification  RelationSpecification              0..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasMemberRelation
#################
Index Entry Relations  that comprise the relationship structure


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  =================  ===========
Name      Type               Cardinality
========  =================  ===========
realizes  RelationStructure  0..n
========  =================  ===========


realizes
########
Class of the Collection pattern realized by this class.



