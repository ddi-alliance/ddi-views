.. _ClassificationIndex:


ClassificationIndex
*******************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A Classification Index is an ordered list (alphabetical, in code order etc) of Classification Index Entries. A Classification Index can relate to one particular or to several Statistical Classifications. [GSIM Statistical Classification Model]


Synonyms
========



Explanatory notes
=================
A Classification Index shows the relationship between text found in statistical data sources (responses to survey questionnaires, administrative records) and one or more Statistical Classifications. A Classification Index may be used to assign the codes for Classification Items to observations in statistical collections. A Statistical Classification is a subtype of a Structured Collection. [GSIM Statistical Classification Model] Note that a GSIM Node is the equivalent of a DDI Member and a GSIM Node Set is a DDI Collection.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst