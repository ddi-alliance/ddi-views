.. _fields:


SubstantiveValueDomain - Properties and Relationships
*****************************************************





Relationships
=============

=====================  ===========================  ===========
Name                   Type                         Cardinality
=====================  ===========================  ===========
describedValueDomain   ValueAndConceptDescription   0..n
enumeratedValueDomain  EnumerationDomain            0..n
takesConceptsFrom      SubstantiveConceptualDomain  0..n
=====================  ===========================  ===========


describedValueDomain
####################
A formal description of the set of valid values - for described value domains.




enumeratedValueDomain
#####################
Any subtype of an enumeration domain enumerating the set of valid values.




takesConceptsFrom
#################
Corresponding conceptual definition given by an SubstantiveConceptualDomain.



