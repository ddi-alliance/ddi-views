.. _fields:


ValueDomain - Properties and Relationships
******************************************





Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
displayLabel         LabelForDisplay                    0..n
recommendedDataType  ExternalControlledVocabularyEntry  0..n
===================  =================================  ===========


displayLabel
############
A display label for the object. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


recommendedDataType
###################
The data types that are recommended for use with this domain

