.. _ValueDomain:


ValueDomain
***********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The permitted range of values for a characteristic of a variable. [GSIM 1.1]


Synonyms
========
BUT NOT Grid/Numeric/Code/*_ResponseDomain [DDI-L/Questions] - this is "ResponseDomain"


Explanatory notes
=================
The values can be described by enumeration or by an expression. Value domains can be either substantive/sentinel, or described/enumeration


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst