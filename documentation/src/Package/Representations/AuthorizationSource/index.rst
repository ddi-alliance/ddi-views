.. _AuthorizationSource:


AuthorizationSource
*******************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Identifies the authorizing agency and allows for the full text of the authorization (law, regulation, or other form of authorization).


Synonyms
========
Use for the Case Law, Case Law Description, and Case Law Date properties in ClassificationItem


Explanatory notes
=================
Supports requirements for some statistical offices to identify the agency or law authorizing the collection or management of data or metadata.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst