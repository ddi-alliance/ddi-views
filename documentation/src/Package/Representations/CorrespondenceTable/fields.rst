.. _fields:


CorrespondenceTable - Properties and Relationships
**************************************************





Properties
==========

==============  =========  ===========
Name            Type       Cardinality
==============  =========  ===========
correspondence  Map        0..n
effectiveDates  DateRange  0..1
==============  =========  ===========


correspondence
##############
Set of mappings between nodes that participate in the correspondence.


effectiveDates
##############
Effective period of validity of the CorrespondenceTable. The correspondence table expresses the relationships between the two NodeSets as they existed on the period specified in the table.




Relationships
=============

===============  =========================  ===========
Name             Type                       Cardinality
===============  =========================  ===========
contactPerson    Agent                      0..n
hasPublication   ExternalMaterial           0..n
maintenanceUnit  Agent                      0..n
maps             StatisticalClassification  0..n
owner            Agent                      0..n
realizes         Comparison                 0..n
sourceLevel      LevelStructure             0..n
targetLevel      LevelStructure             0..n
===============  =========================  ===========


contactPerson
#############
The person(s) who may be contacted for additional information about the Correspondence Table. Can be an individual or organization.




hasPublication
##############
A list of the publications in which the Correspondence Table has been published.




maintenanceUnit
###############
The unit or group of persons who are responsible for the Correspondence Table, i.e. for maintaining and updating it.




maps
####
The Statistical Classification(s) from which the correspondence is made.




owner
#####
The statistical office, other authority or section that created and maintains the Correspondence Table. A Correspondence Table may have several owners.




realizes
########
Class of the Collection pattern realized by this class. 




sourceLevel
###########
Level from which the correspondence is made. Correspondences might be restricted to a certain Level in the NodeSet. In this case, target items are assigned only to source items on the given level. If no level is indicated, source items can be assigned to any level of the target NodeSet.




targetLevel
###########
Level to which the correspondence is made. Correspondences might be restricted to a certain Level in the NodeSet. In this case, target items are assigned only to source items on the given level. If no level is indicated, target items can be assigned to any level of the source NodeSet.



