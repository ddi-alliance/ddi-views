.. _fields:


TextResponseDomain - Properties and Relationships
*************************************************





Properties
==========

=================  ===========  ===========
Name               Type         Cardinality
=================  ===========  ===========
maximumLength      Integer      0..1
minimumLength      Integer      0..1
regularExpression  TypedString  0..1
=================  ===========  ===========


maximumLength
#############
The maximum number of characters allowed.


minimumLength
#############
The minimum number of characters allowed.


regularExpression
#################
A regular expression limiting the allowed characters or character order of the content. Use typeOfContent to specify the syntax of the regularExpression found in content.

