.. _fields:


InstrumentCode - Properties and Relationships
*********************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
purposeOfCode    ExternalControlledVocabularyEntry  0..1
usesCommandCode  CommandCode                        0..1
===============  =================================  ===========


purposeOfCode
#############
The purpose of the code (e.g., quality control, edit check, checksums, compute filler text, compute values for use in administering the instrument)


usesCommandCode
###############
Describes the code used to execute the command using the options of inline textual description, inline code, and/or an external file.

