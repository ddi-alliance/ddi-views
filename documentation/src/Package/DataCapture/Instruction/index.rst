.. _Instruction:


Instruction
***********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides the content and description of data capture instructions. Contains the "how to" information for administering an instrument.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst