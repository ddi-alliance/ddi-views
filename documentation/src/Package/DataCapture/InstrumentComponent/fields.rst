.. _fields:


InstrumentComponent - Properties and Relationships
**************************************************





Relationships
=============

==============  ===========  ===========
Name            Type         Cardinality
==============  ===========  ===========
hasExternalAid  ExternalAid  0..n
hasInstruction  Instruction  0..n
==============  ===========  ===========


hasExternalAid
##############
Any external object used to clarify, inform, or support the role of the instrument component




hasInstruction
##############
An instrument compoment can have zero to many instructions



