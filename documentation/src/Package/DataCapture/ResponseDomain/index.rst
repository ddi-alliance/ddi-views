.. _ResponseDomain:


ResponseDomain
**************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
The possible list of values that are allowed by a Capture.


Synonyms
========
GSIM & DDI: ResponseDomain


Explanatory notes
=================
Identifies both the sentinel and substantive value domains used for capturing the response to a question


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst