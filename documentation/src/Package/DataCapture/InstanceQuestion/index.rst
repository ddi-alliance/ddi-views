.. _InstanceQuestion:


InstanceQuestion
****************


Extends
=======
:ref:`InstrumentComponent`


Definition
==========
An instance question is an instantiation of a represented question,to be used as an Act in the process steps that define a survey questionnaire.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst