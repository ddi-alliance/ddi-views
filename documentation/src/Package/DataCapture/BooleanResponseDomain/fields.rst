.. _fields:


BooleanResponseDomain - Properties and Relationships
****************************************************





Relationships
=============

===========  ========  ===========
Name         Type      Cardinality
===========  ========  ===========
forCategory  Category  0..n
===========  ========  ===========


forCategory
###########
The category to which a boolean response is requested.



