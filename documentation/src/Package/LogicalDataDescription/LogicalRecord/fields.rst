.. _fields:


LogicalRecord - Properties and Relationships
********************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
contains   InstanceVariableIndicator      0..n
isOrdered  Boolean                        0..1
name       ObjectName                     0..n
purpose    InternationalStructuredString  0..1
type       CollectionType                 0..1
=========  =============================  ===========


contains
########
Specifies contents of Unit Record as Instance Variable values.


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

===============  ================  ===========
Name             Type              Cardinality
===============  ================  ===========
definingConcept  Concept           0..n
realizes         SimpleCollection  0..n
===============  ================  ===========


definingConcept
###############
The conceptual basis for the collection of members.




realizes
########
Specifies the subtype of collection realized by this class



