.. _fields:


DataPoint - Properties and Relationships
****************************************





Relationships
=============

=============  ================  ===========
Name           Type              Cardinality
=============  ================  ===========
hasDatum       Datum             0..n
isDescribedBy  InstanceVariable  0..n
realizes       CollectionMember  0..n
=============  ================  ===========


hasDatum
########
The Datum populating the DataPoint.




isDescribedBy
#############
The InstanceVariable delimits the values which can populate a DataPoint.




realizes
########
Classes which realize the class Member fufill the role of Member e.g. they may have relations defined on them, for example one DataPoint follows another in a  Record.



