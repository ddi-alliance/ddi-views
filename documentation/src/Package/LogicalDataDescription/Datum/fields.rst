.. _fields:


Datum - Properties and Relationships
************************************





Properties
==========

==============  ===========  ===========
Name            Type         Cardinality
==============  ===========  ===========
representation  ValueString  1..1
==============  ===========  ===========


representation
##############
Specification of the type of Signification




Relationships
=============

===========  ================  ===========
Name         Type              Cardinality
===========  ================  ===========
isBoundedBy  InstanceVariable  0..n
===========  ================  ===========


isBoundedBy
###########
A Datum is bounded by an InstanceVariable. The Datum is drawn from a set of values, either substantive or sentinel described by the value domain of the InstanceVariable 



