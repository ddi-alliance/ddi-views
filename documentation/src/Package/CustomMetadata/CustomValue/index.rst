.. _CustomValue:


CustomValue
***********


Extends
=======
:ref:`Identifiable`


Definition
==========
An instance of a key, value pair for a particular key.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst