.. _VocabularyRelationStructure:


VocabularyRelationStructure
***************************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Contains the Vocabulary Relations used to structure the ControlledVocabulary


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst