.. _examples:


CustomStructure - Examples
**************************


The set of CustomItems which are to be used to document the OMB required information for a question. The set of CustomItems used to describe an openEHR blood pressure protocol.