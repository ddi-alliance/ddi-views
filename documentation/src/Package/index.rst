========
Packages
========
			
.. image:: ../../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   StructuredDataTypes/index.rst
   Conceptual/index.rst
   SimpleCodebook/index.rst
   SimpleMethodologyOverview/index.rst
   MethodologyPattern/index.rst
   SignificationPattern/index.rst
   RegularExpressions/index.rst
   ProcessPattern/index.rst
   GeographicClassification/index.rst
   FormatDescription/index.rst
   CustomMetadata/index.rst
   SamplingMethodology/index.rst
   StudyRelated/index.rst
   Enumerations/index.rst
   BusinessWorkflow/index.rst
   Utility/index.rst
   Workflows/index.rst
   Methodologies/index.rst
   Discovery/index.rst
   DataCapture/index.rst
   CollectionsPattern/index.rst
   Identification/index.rst
   LogicalDataDescription/index.rst
   Primitives/index.rst
   Agents/index.rst
   Representations/index.rst
