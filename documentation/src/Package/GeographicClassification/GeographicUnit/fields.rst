.. _fields:


GeographicUnit - Properties and Relationships
*********************************************





Properties
==========

==============  ===================  ===========
Name            Type                 Cardinality
==============  ===================  ===========
geographicTime  DateRange            0..1
precedes        SpatialRelationship  0..n
supercedes      SpatialRelationship  0..n
==============  ===================  ===========


geographicTime
##############
The period for which the Geographic Unit is valid. Note that the geographic extent, name, or composition change within the valid period of this unit.


precedes
########
The Geographic Unit is superceded by (precedes immediately in time) the described geographic unit. Repeat for multiple geographic units.


supercedes
##########
The Geographic Unit supercedes (follows immediately in time) the described geographic unit. Repeat for multiple geographic units.




Relationships
=============

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
hasGeographicExtent  GeographicExtent  0..n
===================  ================  ===========


hasGeographicExtent
###################
Describes the geographic extent or coverage of the geographic unit during the specified period of time. 



