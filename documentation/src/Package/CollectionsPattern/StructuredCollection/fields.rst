.. _fields:


StructuredCollection - Properties and Relationships
***************************************************





Relationships
=============

==============  =================  ===========
Name            Type               Cardinality
==============  =================  ===========
isStructuredBy  RelationStructure  0..n
==============  =================  ===========


isStructuredBy
##############
A relation structure that provides complex organization of the collection



