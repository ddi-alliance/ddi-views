.. _StructuredCollection:


StructuredCollection
********************


Extends
=======
:ref:`SimpleCollection`


Definition
==========
Structured Collection container extends a Simple Collection allowing it to optionally have an RelationshpStructure to model hierarchies and nesting or more complex network structures. Like Simple Collection it is also a subtype of Member to allow for nested collections. A Collection can be described directly as having unordered members or an indexed array of members through a set of MemberIndicators.


Synonyms
========



Explanatory notes
=================
Members have to belong to some Collection, except in the case of nested Collections where the top level Collection is a Member that doesn't belong to any Collection. If a Collection is ordered as a simple list that ordering can be indicated by an index property of each MemberIndicator. If the Collection is unordered, the index is not necessary. Collection is not extended, it is realized (see Collection Pattern documentation).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst