.. _SimpleCollection:


SimpleCollection
****************


Extends
=======
:ref:`CollectionMember`


Definition
==========
Simple Collection container (set or bag) that may be unordered or ordered using Member Indicator. This collection type is limited to expressing only unordered sets or bags, and simple sequences. Use when there is a need to limit the organization of the collection to one of these forms of organization


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst