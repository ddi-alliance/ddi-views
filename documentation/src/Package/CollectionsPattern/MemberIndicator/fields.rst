.. _fields:


MemberIndicator - Properties and Relationships
**********************************************





Properties
==========

=====  =======  ===========
Name   Type     Cardinality
=====  =======  ===========
index  Integer  0..1
=====  =======  ===========


index
#####
Index number expressed as an integer. The position of the member in an ordered array. Optional for unordered Collections.




Relationships
=============

======  ================  ===========
Name    Type              Cardinality
======  ================  ===========
member  CollectionMember  0..n
======  ================  ===========


member
######
Member of the collection.



