.. _MemberIndicator:


MemberIndicator
***************



Definition
==========
Provides ability to declare an optional sequence or index order to a CollectionMember.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst