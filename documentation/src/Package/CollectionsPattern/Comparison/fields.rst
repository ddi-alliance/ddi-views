.. _fields:


Comparison - Properties and Relationships
*****************************************





Properties
==========

==============  =============  ===========
Name            Type           Cardinality
==============  =============  ===========
correspondence  ComparisonMap  0..n
==============  =============  ===========


correspondence
##############
A map defining the match relationship and the members in the relationship




Relationships
=============

====  ================  ===========
Name  Type              Cardinality
====  ================  ===========
maps  SimpleCollection  0..n
====  ================  ===========


maps
####
The collection(s) contianing the mapped members.



