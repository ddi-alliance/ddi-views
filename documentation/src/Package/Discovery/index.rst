*********
Discovery
*********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Access/index.rst
   BoundingBox/index.rst
   Coverage/index.rst
   SpatialCoverage/index.rst
   TemporalCoverage/index.rst
   TopicalCoverage/index.rst



Graph
=====

.. graphviz:: /images/graph/Discovery/Discovery.dot