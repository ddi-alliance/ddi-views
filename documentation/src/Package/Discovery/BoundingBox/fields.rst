.. _fields:


BoundingBox - Properties and Relationships
******************************************





Properties
==========

=============  ====  ===========
Name           Type  Cardinality
=============  ====  ===========
eastLongitude  Real  1..1
northLatitude  Real  1..1
southLatitude  Real  1..1
westLongitude  Real  1..1
=============  ====  ===========


eastLongitude
#############
The easternmost coordinate expressed as a decimal between the values of -180 and 180 degrees 


northLatitude
#############
The northernmost coordinate expressed as a decimal between the values of -90 and 90 degrees.


southLatitude
#############
The southermost latitude expressed as a decimal between the values of -90 and 90 degrees


westLongitude
#############
The westernmost coordinate expressed as a decimal between the values of -180 and 180 degrees 

