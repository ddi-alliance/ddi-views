.. _Coverage:


Coverage
********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Coverage information for an annotated object. Includes coverage information for temporal, topical, and spatial coverage.


Synonyms
========



Explanatory notes
=================
Coverage is a container for the more specific temporal, spatial, and topical coverages to which it refers.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst