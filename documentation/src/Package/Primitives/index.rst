**********
Primitives
**********

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2




Graph
=====

.. graphviz:: /images/graph/Primitives/Primitives.dot