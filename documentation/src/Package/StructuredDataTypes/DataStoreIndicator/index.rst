.. _DataStoreIndicator:


DataStoreIndicator
******************



Definition
==========
Member Indicator for use with member type DataStore


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst