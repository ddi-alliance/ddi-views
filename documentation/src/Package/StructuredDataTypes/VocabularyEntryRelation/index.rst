.. _VocabularyEntryRelation:


VocabularyEntryRelation
***********************



Definition
==========
Relation of Vocabulary Entries as defined using RelationSpecification


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst