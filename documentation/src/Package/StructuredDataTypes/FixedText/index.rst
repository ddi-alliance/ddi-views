.. _FixedText:


FixedText
*********


Extends
=======
:ref:`LanguageSpecificStructuredStringType`


Definition
==========
The static portion of the text expressed as a StructuredString with the ability to preserve whitespace if critical to the understanding of the content.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst