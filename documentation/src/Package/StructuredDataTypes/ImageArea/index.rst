.. _ImageArea:


ImageArea
*********



Definition
==========
Defines the shape and area of an image used as part of a location representation. The shape is defined as a Rectangle, Circle, or Polygon and content provides the information required to define it in the form of a comma-delimited list of x,y coordinates, listed as a set of adjacent points for rectangles and polygons, and as a center-point and a radius for circles (x,y,r).


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst