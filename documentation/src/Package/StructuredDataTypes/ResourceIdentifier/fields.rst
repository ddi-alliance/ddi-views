.. _fields:


ResourceIdentifier - Properties and Relationships
*************************************************





Properties
==========

=====================  =================================  ===========
Name                   Type                               Cardinality
=====================  =================================  ===========
typeOfRelatedResource  ExternalControlledVocabularyEntry  0..n
=====================  =================================  ===========


typeOfRelatedResource
#####################
The type of relationship between the annotated object and the related resource. Standard usage may include: describesDate, isDescribedBy,  isFormatOf, isPartOf, isReferencedBy, isReplacedBy, isRequiredBy, isVersionOf, references, replaces, requires, etc.

