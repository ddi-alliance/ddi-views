.. _fields:


NumberRangeValue - Properties and Relationships
***********************************************





Properties
==========

============  =======  ===========
Name          Type     Cardinality
============  =======  ===========
decimalValue  Real     0..1
isInclusive   Boolean  0..1
============  =======  ===========


decimalValue
############
Bounding value expressed as an xs:double


isInclusive
###########
Indicates that the value is included in the range. Set to false if the range includes numbers up to but no including the designated value.

