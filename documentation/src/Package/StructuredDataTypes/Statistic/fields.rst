.. _fields:


Statistic - Properties and Relationships
****************************************





Properties
==========

==================  ===================  ===========
Name                Type                 Cardinality
==================  ===================  ===========
computationBase     ComputationBaseList  0..1
content             Real                 0..1
isWeighted          Boolean              0..1
typeOfNumericValue  String               0..1
==================  ===================  ===========


computationBase
###############
Defines the cases included in determining the statistic. The options are total=all cases, valid and missing (invalid); validOnly=Only valid values, missing (invalid) are not included in the calculation; missingOnly=Only missing (invalid) cases included in the calculation.


content
#######
The value of the statistic expressed as a decimal, float, or double.


isWeighted
##########
Set to "true" if the statistic is weighted using the weight designated in VariableStatistics.


typeOfNumericValue
##################
Indicate the type of numeric value as decimal, float, double

