.. _fields:


SpatialLine - Properties and Relationships
******************************************





Properties
==========

===============  =================================  ===========
Name             Type                               Cardinality
===============  =================================  ===========
lineLinkCode     String                             0..1
point            SpatialPoint                       2..n
shapeFileFormat  ExternalControlledVocabularyEntry  0..1
uri              anyURI                             0..1
===============  =================================  ===========


lineLinkCode
############
The lineLinkCode is the identifier of the specific line within the file. 


point
#####
A geographic point defined by a latitude and longitude. A minimum of 2 points is required in order to define the line. 


shapeFileFormat
###############
The format of the shape file existing at the location indicated by the sibling ExternalURI element.


uri
###
A URI for the spatial file containing the line

