.. _fields:


PrivateImage - Properties and Relationships
*******************************************





Properties
==========

==============  =================================  ===========
Name            Type                               Cardinality
==============  =================================  ===========
effectiveDates  DateRange                          0..1
privacy         ExternalControlledVocabularyEntry  0..1
==============  =================================  ===========


effectiveDates
##############
The period for which this image is effective/valid.


privacy
#######
Specify the level privacy for the image as public, restricted, or private. Supports the use of an external controlled vocabulary.

