.. _CategoryStatistic:


CategoryStatistic
*****************



Definition
==========
Statistics related to a specific category of an InstanceVariable within a data set.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst