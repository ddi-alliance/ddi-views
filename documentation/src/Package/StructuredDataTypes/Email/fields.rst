.. _fields:


Email - Properties and Relationships
************************************





Properties
==========

==============  =================================  ===========
Name            Type                               Cardinality
==============  =================================  ===========
effectiveDates  DateRange                          0..1
internetEmail   String                             0..1
isPreferred     Boolean                            0..1
privacy         ExternalControlledVocabularyEntry  0..1
typeOfEmail     ExternalControlledVocabularyEntry  0..1
==============  =================================  ===========


effectiveDates
##############
Time period for which the e-mail address is valid.


internetEmail
#############
The email address expressed as a string (should follow the Internet format specification - RFC 5322) e.g. user@server.ext, more complex and flexible examples are also supported by the format.


isPreferred
###########
Set to true if this is the preferred email


privacy
#######
Indicates the level of privacy


typeOfEmail
###########
Code indicating the type of e-mail address. Supports the use of an external controlled vocabulary. (e.g. home, office)

