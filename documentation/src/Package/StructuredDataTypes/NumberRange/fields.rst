.. _fields:


NumberRange - Properties and Relationships
******************************************





Properties
==========

==============  ======================  ===========
Name            Type                    Cardinality
==============  ======================  ===========
displayLabel    LabelForDisplay         0..n
highCode        NumberRangeValue        0..1
highCodeDouble  DoubleNumberRangeValue  0..1
isBottomCoded   Boolean                 1..1
isTopCoded      Boolean                 1..1
lowCode         NumberRangeValue        0..1
lowCodeDouble   DoubleNumberRangeValue  0..1
regExp          String                  0..1
==============  ======================  ===========


displayLabel
############
A display label for the number range. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


highCode
########
High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.


highCodeDouble
##############
High or top value on the range expressed as an xs:double within an isInclusive Boolean flag.


isBottomCoded
#############
Default value is False. The Low or bottom code represents the value expressed and anything lower.


isTopCoded
##########
Default value is False. The High or top code represents the value expressed and anything higher.


lowCode
#######
High or top value on the range expressed as an xs:decimal within an isInclusive Boolean flag.


lowCodeDouble
#############
Low or bottom value on the range expressed as an xs:double within an isInclusive Boolean flag.


regExp
######
Regular expression defining the allowed syntax of the number.

