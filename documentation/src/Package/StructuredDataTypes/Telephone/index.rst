.. _Telephone:


Telephone
*********



Definition
==========
Details of a telephone number including the number, type of number, a privacy setting and an indication of whether this is the preferred contact number.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst