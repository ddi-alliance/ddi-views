.. _fields:


LanguageSpecificStringType - Properties and Relationships
*********************************************************





Properties
==========

=========================  ================  ===========
Name                       Type              Cardinality
=========================  ================  ===========
content                    String            1..1
isTranslatable             Boolean           0..1
isTranslated               Boolean           0..1
language                                     0..1
scope                      String            0..1
translationDate            UnlimitedNatural  0..1
translationSourceLanguage                    0..n
=========================  ================  ===========


content
#######
Value of this string


isTranslatable
##############
Indicates whether content is translatable (true) or not (false). An example of something that is not translatable would be a MNEMONIC of an object or a number.


isTranslated
############
Indicates whether content is a translation (true) or an original (false).


language
########
Indicates the language of content. Note that language allows for a simple 2 or 3 character language code or a language code extended by a country code , for example en-au for English as used in Australia.


scope
#####
Supports specification of scope for the contained content. Use with the language specification to filter application of content.


translationDate
###############
The date the content was translated. Provision of translation date allows user to verify if translation was available during data collection or other time linked activity.


translationSourceLanguage
#########################
List the language code of the source. Repeat of multiple language sources are used. 

