.. _fields:


BusinessProcessCondition - Properties and Relationships
*******************************************************





Properties
==========

=================  =============================  ===========
Name               Type                           Cardinality
=================  =============================  ===========
dataDescription    InternationalStructuredString  0..n
rejectionCriteria  CommandCode                    0..1
sql                String                         0..1
=================  =============================  ===========


dataDescription
###############
Provides an alternative to or supplements the SQL data description.


rejectionCriteria
#################
Criteria for failing an input dataset


sql
###
SQL SELECT statement that describes the selection of the LogicalRecord from the Dataset




Relationships
=============

=================  =============  ===========
Name               Type           Cardinality
=================  =============  ===========
usesLogicalRecord  LogicalRecord  0..n
=================  =============  ===========


usesLogicalRecord
#################
A Logical Record used by the BusinessProcessCondition in defining the condition.



