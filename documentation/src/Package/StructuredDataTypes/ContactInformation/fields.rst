.. _fields:


ContactInformation - Properties and Relationships
*************************************************





Properties
==========

===================  =======================  ===========
Name                 Type                     Cardinality
===================  =======================  ===========
electronicMessaging  ElectronicMessageSystem  0..n
hasAddress           Address                  0..n
hasEmail             Email                    0..n
hasTelephone         Telephone                0..n
website              WebLink                  0..n
===================  =======================  ===========


electronicMessaging
###################
Electronic messaging other than email


hasAddress
##########
The address for contact.


hasEmail
########
Email contact information


hasTelephone
############
Telephone for contact


website
#######
The URL of the Agent's website

