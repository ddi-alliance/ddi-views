.. _IndexEntryRelation:


IndexEntryRelation
******************



Definition
==========
Relationship between Index Entries defined by RelationSpecification.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst