.. _fields:


TargetSample - Properties and Relationships
*******************************************





Properties
==========

=============  =======  ===========
Name           Type     Cardinality
=============  =======  ===========
isPrimary      Boolean  1..1
targetPercent  Real     0..1
targetSize     Integer  0..1
=============  =======  ===========


isPrimary
#########
Default value is True. The information is for the primary sample unit or universe. Change to False if the information relates to a secondary or sub- sample universe.


targetPercent
#############
The target size of the sample expressed as an integer (70% expressed as .7)


targetSize
##########
The target size of the sample expressed as an integer.




Relationships
=============

============  ========  ===========
Name          Type      Cardinality
============  ========  ===========
fromUniverse  Universe  0..n
hasUnitType   UnitType  0..n
============  ========  ===========


fromUniverse
############
The universe of the sample. Note that when the Design is implemented the Population should be a Population within this Universe.




hasUnitType
###########
Unit type of the sample



