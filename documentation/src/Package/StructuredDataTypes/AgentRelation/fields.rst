.. _fields:


AgentRelation - Properties and Relationships
********************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
effectiveDates            DateRange                          0..1
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


effectiveDates
##############
The effective dates of the relation. A structured DateRange with start and end Date (both with the structure of Date and supporting the use of ISO and non-ISO date structures); Use to relate a period with a start and end date.


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
realizes  MemberRelation  0..n
source    Agent           0..n
target    Agent           0..n
========  ==============  ===========


realizes
########
Uses the pattern of an OrderedPair




source
######
The Parent agent in the hierarchical relationship




target
######
The Child agent in the hierarchical relationship



