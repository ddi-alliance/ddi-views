.. _examples:


AgentRelation - Examples
************************


An Organization (source/parent) employing and Individual (target/child); An Individual (source/parent) supervisory to an Individual (target/child); An Organization (source/parent) overseeing a project (Organization) (target/child). Select appropriate relationship using the controlled vocabulary available through hasRelationshipSpecification.