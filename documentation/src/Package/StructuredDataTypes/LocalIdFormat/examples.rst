.. _examples:


LocalIdFormat - Examples
************************


The name of a variable within a dataset is a localId. Software systems might use their own identifier systems for performance reasons (e.g. incrementing integers).