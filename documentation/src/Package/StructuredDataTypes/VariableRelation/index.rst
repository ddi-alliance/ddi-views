.. _VariableRelation:


VariableRelation
****************



Definition
==========
Ordered relations between any Variables in the Variable Cascade (Conceptual, Represented, Instance)


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst