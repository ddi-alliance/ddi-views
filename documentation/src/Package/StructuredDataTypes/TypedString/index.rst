.. _TypedString:


TypedString
***********



Definition
==========
TypedString combines a typeOfContent with a content defined as a simple string. May be used wherever a simple string needs to support a type definition to clarify its content


Synonyms
========



Explanatory notes
=================
This is a generic type + string where property name and documentation should be used to define any specification for the content. If international structured string content is required use TypedStructuredString


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst