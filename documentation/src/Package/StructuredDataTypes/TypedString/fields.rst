.. _fields:


TypedString - Properties and Relationships
******************************************





Properties
==========

=============  =================================  ===========
Name           Type                               Cardinality
=============  =================================  ===========
content        String                             1..1
typeOfContent  ExternalControlledVocabularyEntry  0..1
=============  =================================  ===========


content
#######
Content of the property expressed as a simple string.


typeOfContent
#############
Optional use of a controlled vocabulary to specifically type the associated content.

