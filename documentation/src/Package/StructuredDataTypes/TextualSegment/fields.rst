.. _fields:


TextualSegment - Properties and Relationships
*********************************************





Properties
==========

==================  ===============  ===========
Name                Type             Cardinality
==================  ===============  ===========
characterParameter  CharacterOffset  0..1
lineParamenter      LineParameter    0..1
==================  ===============  ===========


characterParameter
##################
Specification of the character offset for the beginning and end of the segment.


lineParamenter
##############
Specification of the line and offset for the beginning and end of the segment.

