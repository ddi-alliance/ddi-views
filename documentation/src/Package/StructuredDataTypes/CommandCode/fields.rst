.. _fields:


CommandCode - Properties and Relationships
******************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
description      InternationalStructuredString  0..1
usesCommand      Command                        0..n
usesCommandFile  CommandFile                    0..n
===============  =============================  ===========


description
###########
A description of the purpose and use of the command code provided. Supports multiple languages.


usesCommand
###########
This is an in-line provision of the command itself. It provides the programming language used as well as the command.


usesCommandFile
###############
Identifies and provides a link to an external copy of the command, for example, a SAS Command Code script. Designates the programming language of the command file as well as the URI for the file.

