.. _InternationalIdentifier:


InternationalIdentifier
***********************



Definition
==========
An identifier whose scope of uniqueness is broader than the local archive. Common forms of an international identifier are ISBN, ISSN, DOI or similar designator. Provides both the value of the identifier and the agency who manages it.


Synonyms
========



Explanatory notes
=================
For use in Annotation or other citation format.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst