.. _RangeValue:


RangeValue
**********


Extends
=======
:ref:`ValueString`


Definition
==========
Describes a bounding value of a string.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst