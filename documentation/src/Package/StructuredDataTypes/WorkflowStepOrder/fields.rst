.. _fields:


WorkflowStepOrder - Properties and Relationships
************************************************





Properties
==========

=====  =======  ===========
Name   Type     Cardinality
=====  =======  ===========
index  Integer  1..1
=====  =======  ===========


index
#####
Index value of WorkflowStep in an ordered array




Relationships
=============

===================  ============  ===========
Name                 Type          Cardinality
===================  ============  ===========
indexedWorkflowStep  WorkflowStep  0..n
===================  ============  ===========


indexedWorkflowStep
###################
The subtype of a WorkflowStep occuring at the indexed position within the array. 



