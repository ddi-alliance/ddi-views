.. _fields:


DoubleNumberRangeValue - Properties and Relationships
*****************************************************





Properties
==========

===========  =======  ===========
Name         Type     Cardinality
===========  =======  ===========
doubleValue  Real     0..1
isInclusive  Boolean  0..1
===========  =======  ===========


doubleValue
###########
The bounding value expressed as an xs:double


isInclusive
###########
Indicates that the value is included in the range. Set to false if the range includes numbers up to but no including the designated value.

