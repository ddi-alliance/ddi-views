.. _AgentAssociation:


AgentAssociation
****************



Definition
==========
A basic structure for declaring the name of an Agent inline, reference to an Agent, and role specification. This object is used primarily within Annotation.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst