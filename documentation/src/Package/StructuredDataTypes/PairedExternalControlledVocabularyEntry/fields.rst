.. _fields:


PairedExternalControlledVocabularyEntry - Properties and Relationships
**********************************************************************





Properties
==========

======  =================================  ===========
Name    Type                               Cardinality
======  =================================  ===========
extent  ExternalControlledVocabularyEntry  0..1
term    ExternalControlledVocabularyEntry  1..1
======  =================================  ===========


extent
######
Describes the extent to which the parent term applies for the specific case using an external controlled vocabulary. When associated with a role from the CASRAI Contributor Roles Taxonomy an appropriate vocabulary should be specified as either ‘lead’, ‘equal’, or ‘supporting’.


term
####
The term attributed to the parent class, for example the role of a Contributor.

