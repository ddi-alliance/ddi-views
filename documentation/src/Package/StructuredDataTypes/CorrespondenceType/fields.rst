.. _fields:


CorrespondenceType - Properties and Relationships
*************************************************





Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
commonality          InternationalStructuredString      0..1
commonalityTypeCode  ExternalControlledVocabularyEntry  0..n
difference           InternationalStructuredString      0..1
hasMappingRelation   MappingRelation                    0..1
===================  =================================  ===========


commonality
###########
A description of the common features of the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.


commonalityTypeCode
###################
Commonality expressed as a term or code. Supports the use of an external controlled vocabulary. If repeated, clarify each external controlled vocabulary used.


difference
##########
A description of the differences between the two items using a StructuredString to support multiple language versions of the same content as well as optional formatting of the content.


hasMappingRelation
##################
Allows specification of exact match, close match, or disjoint. These relationships can be further defined by describing commonalities or differences or providing additional controlled vocabulary description of relationship.

