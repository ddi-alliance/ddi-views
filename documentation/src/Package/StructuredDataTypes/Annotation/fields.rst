.. _fields:


Annotation - Properties and Relationships
*****************************************





Properties
==========

======================  =================================  ===========
Name                    Type                               Cardinality
======================  =================================  ===========
alternativeTitle        InternationalString                0..n
contributor             AgentAssociation                   0..n
copyright               InternationalString                0..n
creator                 AgentAssociation                   0..n
date                    AnnotationDate                     0..n
identifier              InternationalIdentifier            0..n
informationSource       InternationalString                0..n
languageOfObject                                           0..n
provenance              InternationalString                0..n
publisher               AgentAssociation                   0..n
recordCreationDate      IsoDateType                        0..1
recordLastRevisionDate  IsoDateType                        0..1
relatedResource         ResourceIdentifier                 0..n
rights                  InternationalString                0..n
subTitle                InternationalString                0..n
summary                 InternationalString                0..1
title                   InternationalString                0..1
typeOfResource          ExternalControlledVocabularyEntry  0..n
versionIdentification   String                             0..1
versioningAgent         AgentAssociation                   0..n
======================  =================================  ===========


alternativeTitle
################
An alternative title by which a data collection is commonly referred, or an abbreviation  for the title.


contributor
###########
The name of a contributing author or creator, who worked in support of the primary creator given above.


copyright
#########
The copyright statement.


creator
#######
Person, corporate body, or agency responsible for the substantive and intellectual content of the described object.


date
####
A date associated with the annotated object (not the coverage period). Use typeOfDate to specify the type of date such as Version, Publication, Submitted, Copyrighted, Accepted, etc.


identifier
##########
An identifier or locator. Contains identifier and Managing agency (ISBN, ISSN, DOI, local archive). Indicates if it is a URI.


informationSource
#################
The name or identifier of source information for the annotated object.


languageOfObject
################
Language of the intellectual content of the described object. Multiple languages are supported by the structure itself as defined in the transformation to specific bindings. Use language codes supported by xs:language which include the 2 and 3 character and extended structures defined by RFC4646 or its successors. Supports multiple language codes.


provenance
##########
A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.


publisher
#########
Person or organization responsible for making the resource available in its present form.


recordCreationDate
##################
Date the record was created


recordLastRevisionDate
######################
Date the record was last revised


relatedResource
###############
Provide the identifier, managing agency, and type of resource related to this object. Use to specify related resources similar to Dublin Core isPartOf and hasPart to indicate collection/series membership for objects where there is an identifiable record. If not an identified object use the relationship to ExternalMaterial using a type that indicates a series description. 


rights
######
Information about rights held in and over the resource. Typically, rights information includes a statement about various property rights associated with the resource, including intellectual property rights.


subTitle
########
Secondary or explanatory title.


summary
#######
A summary description (abstract) of the annotated object.


title
#####
Full authoritative title. List any additional titles for this item as AlternativeTitle.


typeOfResource
##############
Provide the type of the resource. This supports the use of a controlled vocabulary. It should be appropriate to the level of the annotation.


versionIdentification
#####################
Means of identifying the current version of the annotated object. 


versioningAgent
###############
The agent responsible for the version. May have an associated role.

