.. _DataPointRelation:


DataPointRelation
*****************



Definition
==========
Data Point relations used by the Data Point Relation Structure of a Logical Record to describe specific source target Data Points and their relationship


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst