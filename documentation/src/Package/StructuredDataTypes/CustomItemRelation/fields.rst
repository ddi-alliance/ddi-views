.. _fields:


CustomItemRelation - Properties and Relationships
*************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


hasRelationSpecification
########################
The type of relationship between the two CustomItems. The verb in the sentence describing the relationship between the source (subject) and the target (object).


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  ==============  ===========
Name      Type            Cardinality
========  ==============  ===========
realizes  MemberRelation  0..n
source    CustomItem      0..n
target    CustomItem      0..n
========  ==============  ===========


realizes
########
realizes pattern MemberRelationship




source
######
Restricts source object to a CustomItem




target
######
Restricts target object to a CustomItem



