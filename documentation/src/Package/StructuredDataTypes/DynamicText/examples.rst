.. _examples:


DynamicText - Examples
**********************


Purpose: Provide customized number of hours within a question text Input is the OutputParameter from Question "What time did you take this medication?" Literal Text: "During the past " [0] Conditional Text / Command: PastTime=Input; Round PastTime to nearest hour; Round CurrentTime to nearest hour; CurrentTime - PastTime = HOURS; content=HOURS [1] Literal Text: " hours which of the following symptoms have you experienced?" [2] Result in question text for a PastTime= 06:10; CurrentTime=14:54 "During the past 9 hours which of the following symptoms have you experienced?"