.. _DynamicTextContent:


DynamicTextContent
******************



Definition
==========
Abstract type existing as the head of a substitution group. May be replaced by any valid member of the substitution group TextContent. Provides the common property of purpose to all members using TextContent as an extension base. Provides a Purpose for the content and ordering information within the Dynamic Text. May be a LiteralText or ConditionalText.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst