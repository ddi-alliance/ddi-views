.. _fields:


DynamicTextContent - Properties and Relationships
*************************************************





Properties
==========

=============  =============================  ===========
Name           Type                           Cardinality
=============  =============================  ===========
orderPosition  Integer                        0..1
purpose        InternationalStructuredString  0..1
=============  =============================  ===========


orderPosition
#############
Provides the relative order of TextContent objects in a dynamic text with more than one TextContent object. Uses integer value.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex

