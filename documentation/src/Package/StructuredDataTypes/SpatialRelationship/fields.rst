.. _fields:


SpatialRelationship - Properties and Relationships
**************************************************





Properties
==========

===============================  ============================  ===========
Name                             Type                          Cardinality
===============================  ============================  ===========
eventDate                        Date                          0..1
hasSpatialObjectPair             SpatialObjectPairs            0..1
hasSpatialRelationSpecification  SpatialRelationSpecification  0..1
===============================  ============================  ===========


eventDate
#########
The date of the relationship change.


hasSpatialObjectPair
####################
Provides the types of the two spatial objects.


hasSpatialRelationSpecification
###############################
Defines the relationship of the two spatial objects




Relationships
=============

=========  ==============  ===========
Name       Type            Cardinality
=========  ==============  ===========
relatesTo  GeographicUnit  0..n
=========  ==============  ===========


relatesTo
#########
The geographic unit related to by the parent geographic unit



