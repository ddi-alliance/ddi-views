.. _SpatialRelationship:


SpatialRelationship
*******************



Definition
==========
Used to specify a relationship between one spatial object and another. Includes definition of the spatial object types being related, the spatial relation specification, and the event date.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst