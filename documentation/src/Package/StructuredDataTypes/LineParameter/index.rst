.. _LineParameter:


LineParameter
*************



Definition
==========
Specification of the line and offset for the beginning and end of the segment.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst