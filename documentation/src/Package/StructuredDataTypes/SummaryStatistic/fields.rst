.. _fields:


SummaryStatistic - Properties and Relationships
***********************************************





Properties
==========

======================  =================================  ===========
Name                    Type                               Cardinality
======================  =================================  ===========
hasStatistic            Statistic                          0..n
typeOfSummaryStatistic  ExternalControlledVocabularyEntry  0..1
======================  =================================  ===========


hasStatistic
############
The value of the statistics and whether it is weighted and/or includes missing values. May expressed as xs:decimal and/or xs:double


typeOfSummaryStatistic
######################
Type of summary statistic, such as count, mean, mode, median, etc. Supports the use of an external controlled vocabulary. DDI strongly recommends the use of a controlled vocabulary.

