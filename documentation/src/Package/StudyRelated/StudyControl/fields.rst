.. _fields:


StudyControl - Properties and Relationships
*******************************************





Relationships
=============

========  =====  ===========
Name      Type   Cardinality
========  =====  ===========
hasStudy  Study  0..n
========  =====  ===========


hasStudy
########
Study that is the target of the StudyControl



