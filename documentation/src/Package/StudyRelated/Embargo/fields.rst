.. _fields:


Embargo - Properties and Relationships
**************************************





Properties
==========

============  =============================  ===========
Name          Type                           Cardinality
============  =============================  ===========
displayLabel  LabelForDisplay                0..n
embargoDates  DateRange                      0..1
rationale     InternationalStructuredString  0..1
============  =============================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text. 


embargoDates
############
Provides the date range of the embargo


rationale
#########
Explanation of the reasons some decision was made or some object exists. Supports the use of multiple languages and structured text.




Relationships
=============

================  =====  ===========
Name              Type   Cardinality
================  =====  ===========
enforcementAgent  Agent  0..n
responsibleAgent  Agent  0..n
================  =====  ===========


enforcementAgent
################
Agent responsible for enforcing the embargo




responsibleAgent
################
Agent (Organiztion, Individual, etc.) that is responsible for the embargo



