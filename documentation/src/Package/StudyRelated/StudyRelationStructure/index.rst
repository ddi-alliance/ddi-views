.. _StudyRelationStructure:


StudyRelationStructure
**********************


Extends
=======
:ref:`Identifiable`


Definition
==========
Allows for a complex nested structure for a Study Series


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst