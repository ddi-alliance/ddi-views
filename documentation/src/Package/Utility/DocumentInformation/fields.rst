.. _fields:


DocumentInformation - Properties and Relationships
**************************************************





Properties
==========

=================  ====================  ===========
Name               Type                  Cardinality
=================  ====================  ===========
contentCoverage    TypedDescriptiveText  0..n
hasPrimaryContent  UnlimitedNatural      0..1
isPublished        Boolean               1..1
ofType             DDI4Version           1..1
=================  ====================  ===========


contentCoverage
###############
Allows for the addition of a Purpose, Table of Contents, List of Variables, or other forms of information more specific than a general abstract. Provides both the content (using description) and a type identification using an External Controlled Vocabulary Entry.


hasPrimaryContent
#################
A whitespace-delimited list of the DDI URN identifiers of the objects contained in an XML instance or RDF graph which could be considered the primary objects or entry points. In a Codebook View, for example, the top-level Codebook object, fo example. In some views, such as the Agents View, there my be more than one primary object (Individuals, Organizations, and Machines in this case).


isPublished
###########
Default value is False. Indicates that the metadata instance will not be changed without versioning, and is a stable target for referencing.


ofType
######
Automatically brings in the version number for of DDI 4




Relationships
=============

==========================  ==================  ===========
Name                        Type                Cardinality
==========================  ==================  ===========
hasDocumentCoverage         Coverage            0..n
hasExternalMaterial         ExternalMaterial    0..n
hasFundingSource            FundingInformation  0..n
hasLocalAccessControl       Access              0..n
hasPersistentAccessControl  Access              0..n
==========================  ==================  ===========


hasDocumentCoverage
###################
Describes the coverage of the document as a whole.




hasExternalMaterial
###################
Allows for listing information on any material external to the DDI metadata instance that is related to it.




hasFundingSource
################
Describes funding source by description, grant number, and link to funding agent




hasLocalAccessControl
#####################
Access control imposed by the local distributor. For example, distribution only to registered users of a system or members of an association. Provides information on access restrictions as well as the process to gain access.




hasPersistentAccessControl
##########################
Access control that will persist over time, generally imposed by the producer (i.e. embargo limits, confidentiality constraints). Provides information on access restrictions as well as the process to gain access.



