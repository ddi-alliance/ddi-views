*******
Utility
*******

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   DDI4Version/index.rst
   DocumentInformation/index.rst
   ExternalMaterial/index.rst
   FundingInformation/index.rst



Graph
=====

.. graphviz:: /images/graph/Utility/Utility.dot