.. _ExternalMaterial:


ExternalMaterial
****************


Extends
=======
:ref:`Identifiable`


Definition
==========
ExternalMaterial describes the location, structure, and relationship to the DDI metadata instance for any material held external to that instance. This includes citations to such material, an external reference to a URL (or other URI), and a statement about the relationship between the cited ExternalMaterial the contents of the DDI instance. It should be used as follows:as a target object from a relationship which clarifies its role within a class; or as the target of a relatedResource within an annotation.


Synonyms
========



Explanatory notes
=================
Within the DDI model, ExternalMaterial is used as an extension base for specific external materials found such as an External Aid. It is used as a base for specifically related material (e.g. ExternalAid) by creating a relationship whose name clarifies the purpose of the related material.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst