.. _Signified:


Signified
*********


Extends
=======
:ref:`CollectionMember`


Definition
==========
Concept or object denoted by the signifier associated to a sign.


Synonyms
========



Explanatory notes
=================
Concept or object represented by the sign.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst