.. _Signifier:


Signifier
*********



Definition
==========
Concept whose extension includes perceivable objects.


Synonyms
========



Explanatory notes
=================
A signifier has the potential to refer to an object. In this case, the referring signifier is a label. If that object is a concept, then the referring signifier is a designation. A label is a representation of an object by a signifier which denotes it. For instance, the token http://www.bls.gov is a label for the web site maintained for the US Bureau of Labor Statistics. The label is also an identifier, since it is intended to dereference the BLS web site. It is a locator, since the HTTP service is associated with it. Finally, ‘Dan Gillman’ is a name for a co-author of this paper, since the token is linguistic.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst