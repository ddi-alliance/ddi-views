.. _fields:


StandardWeight - Properties and Relationships
*********************************************





Properties
==========

===================  ====  ===========
Name                 Type  Cardinality
===================  ====  ===========
standardWeightValue  Real  0..1
===================  ====  ===========


standardWeightValue
###################
Provides the standard weight used for weighted analysis of data expressed as an xs:float. This element is referenced by the variable and/or statistics calculated using the standard weight.

