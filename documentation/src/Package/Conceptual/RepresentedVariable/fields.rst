.. _fields:


RepresentedVariable - Properties and Relationships
**************************************************





Properties
==========

===================  =================================  ===========
Name                 Type                               Cardinality
===================  =================================  ===========
hasIntendedDataType  ExternalControlledVocabularyEntry  0..1
unitOfMeasurement    String                             0..1
===================  =================================  ===========


hasIntendedDataType
###################
The data type intended to be used by this variable. Supports the optional use of an external controlled vocabulary.


unitOfMeasurement
#################
The unit in which the data values are measured (kg, pound, euro).




Relationships
=============

===================================  ======================  ===========
Name                                 Type                    Cardinality
===================================  ======================  ===========
basedOnConceptualVariable            ConceptualVariable      0..n
drawsFrom                            Universe                0..n
takesPlatformSpecificSentinelValues  SentinelValueDomain     0..n
takesSubstantiveValuesFrom           SubstantiveValueDomain  0..n
===================================  ======================  ===========


basedOnConceptualVariable
#########################
The ConceptualVariable that can be shared by a set of multiple RepresentedVariables. Indicates comparability in  ConceptualDomain and UnitType. 




drawsFrom
#########
The defined class of people, entities, events, or objects to be measured.




takesPlatformSpecificSentinelValues
###################################
A RepresentedVariable may have more than one sets of SentinelValueDomains, one for each type of software platform on which related InstanceVariables might be instantiated. All of the SentinelValueDomains must have SentinelConceptualDomains that correspond exactly. This allows codes for missing values to be explicitly matched across platforms.




takesSubstantiveValuesFrom
##########################
The substantive representation (SubstantiveValueDomain) of the variable. This is equivalent to the relationship "Measures" in GSIM although GSIM makes no distinction between substantive and sentinel values



