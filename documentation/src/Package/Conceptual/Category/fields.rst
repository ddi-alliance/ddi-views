.. _fields:


Category - Properties and Relationships
***************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
descriptiveText  InternationalStructuredString  0..1
===============  =============================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.

