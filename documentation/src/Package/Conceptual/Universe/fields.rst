.. _fields:


Universe - Properties and Relationships
***************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
descriptiveText  InternationalStructuredString  0..1
isInclusive      Boolean                        0..1
===============  =============================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.


isInclusive
###########
Default value is True. The description statement of a universe is generally stated in inclusive terms such as "All persons with university degree". Occasionally a universe is defined by what it excludes, i.e., "All persons except those with university degree". In this case the value would be changed to "false".




Relationships
=============

===============  ========  ===========
Name             Type      Cardinality
===============  ========  ===========
narrowsUnitType  UnitType  0..n
usesConcept      Concept   0..n
===============  ========  ===========


narrowsUnitType
###############
Reference to the Unit Type that the Universe definition narrows.




usesConcept
###########
Reference to the Concept that is being used



