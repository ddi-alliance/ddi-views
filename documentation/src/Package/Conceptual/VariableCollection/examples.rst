.. _examples:


VariableCollection - Examples
*****************************


Variables within a specific section of a study, Variables related to a specific subject or keyword. Variables at a specified level of development or review.