.. _Population:


Population
**********


Extends
=======
:ref:`Universe`


Definition
==========
Set of specific units (people, entities, objects, events) with specification of time and geography.


Synonyms
========



Explanatory notes
=================
Population is the most specific in the conceptually narrowing hierarchy of UnitType, Universe and Population. Several Populations having differing time and or geography may specialize the same Universe.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst