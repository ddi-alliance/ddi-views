.. _fields:


ConceptualVariable - Properties and Relationships
*************************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
descriptiveText  InternationalStructuredString  0..1
===============  =============================  ===========


descriptiveText
###############
A short natural language account of the characteristics of the object.




Relationships
=============

============================  ===========================  ===========
Name                          Type                         Cardinality
============================  ===========================  ===========
takesSentinelConceptsFrom     SentinelConceptualDomain     0..n
takesSubstantiveConceptsFrom  SubstantiveConceptualDomain  0..n
usesConcept                   Concept                      0..n
usesUnitType                  UnitType                     0..n
============================  ===========================  ===========


takesSentinelConceptsFrom
#########################
Identifies the ConceptualDomain containing the set of sentinel concepts used to describe the  ConceptualVariable.




takesSubstantiveConceptsFrom
############################
Identifies the ConceptualDomain containing the set of substantive concepts used to describe the  ConceptualVariable.




usesConcept
###########
Reference to a Concept that is being used




usesUnitType
############
Identifies the UnitType associated with the ConceptualVariable



