.. _examples:


PhysicalDataSet - Examples
**************************


The PhysicalDataProduct is the entry point for information about a file or other source. It includes information about the name of a file, the structure of segments in a file and the layout of segments.