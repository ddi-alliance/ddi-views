.. _fields:


PhysicalDataSet - Properties and Relationships
**********************************************





Properties
==========

================  ==============================  ===========
Name              Type                            Cardinality
================  ==============================  ===========
contains          PhysicalRecordSegmentIndicator  0..n
isOrdered         Boolean                         0..1
numberOfSegments  Integer                         0..1
overview          InternationalStructuredString   0..1
physicalFileName  String                          0..1
purpose           InternationalStructuredString   0..1
type              CollectionType                  0..1
================  ==============================  ===========


contains
########
A physical description of the data store encompasses a description of its gross record structure. This structure consists of one or more PhysicalRecordSegments and their relationships. Allows for the identification of the member and optionally provides an index for the member within an ordered array 


isOrdered
#########
If members are ordered set to true, if unordered set to false.


numberOfSegments
################
The number of segments in a Physical Data Product


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


physicalFileName
################
Use when multiple physical segments are stored in a single file


purpose
#######
An explanation of the purpose of the physical dataset


type
####
Whether the dataset is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates. 




Relationships
=============

=====================  ==============================  ===========
Name                   Type                            Cardinality
=====================  ==============================  ===========
definingConcept        Concept                         0..n
formatsDataStore       DataStore                       0..n
hasVariableStatistics  VariableStatistics              0..n
isStructuredBy         PhysicalOrderRelationStructure  0..n
realizes               StructuredCollection            0..n
=====================  ==============================  ===========


definingConcept
###############
The conceptual basis for the collection of members.




formatsDataStore
################
Data Store physically represented by the Structure Description. 




hasVariableStatistics
#####################
Variable Statistics related to the Physical Data Set. May be summary or category level statistics.




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses.




realizes
########
Collection class relized by this class



