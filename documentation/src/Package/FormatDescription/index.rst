*****************
FormatDescription
*****************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   DataPointRelationStructure/index.rst
   PhysicalDataSet/index.rst
   PhysicalLayoutRelationStructure/index.rst
   PhysicalOrderRelationStructure/index.rst
   PhysicalRecordSegment/index.rst
   PhysicalSegmentLayout/index.rst
   PhysicalSegmentLocation/index.rst
   SegmentByText/index.rst
   UnitSegmentLayout/index.rst
   ValueMapping/index.rst



Graph
=====

.. graphviz:: /images/graph/FormatDescription/FormatDescription.dot