.. _UnitSegmentLayout:


UnitSegmentLayout
*****************


Extends
=======
:ref:`PhysicalSegmentLayout`


Definition
==========
UnitSegmentLayout supports the description of unit-record ("wide") data sets, where each row in the data set provides the same group of values for variables all relating to a single unit. Each logical column will contain data relating to the values for a single variable.


Synonyms
========



Explanatory notes
=================
This is the classic rectangular data table used by most statistical packages, with rows/cases/observations and columns/variables/measurements. Each cell (DataPoint) in the table is the intersection of a Unit (row) and an InstanceVariable.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst