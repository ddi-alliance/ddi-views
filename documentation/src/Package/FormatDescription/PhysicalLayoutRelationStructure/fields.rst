.. _fields:


PhysicalLayoutRelationStructure - Properties and Relationships
**************************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
criteria                  InternationalStructuredString      0..1
hasMemberRelation         ValueMappingRelation               0..n
hasRelationSpecification  RelationSpecification              0..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


hasMemberRelation
#################
Specifics of relations between InstanceVariables in the PhysicalLayout


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list.  Use if all relations within this relation structure are of the same specification. 


semantic
########
Provides semantic context for the relationship


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  =================  ===========
Name      Type               Cardinality
========  =================  ===========
realizes  RelationStructure  0..n
========  =================  ===========


realizes
########
Class of the Collection pattern realized by this class.



