.. _fields:


Design - Properties and Relationships
*************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
overview  InternationalStructuredString  0..1
========  =============================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.




Relationships
=============

===================  ================  ===========
Name                 Type              Cardinality
===================  ================  ===========
assumesPrecondition  Precondition      0..n
expressesAlgorithm   Algorithm         0..n
implentatedBy        Process           0..n
isDiscussedIn        ExternalMaterial  0..n
specifiesGoal        Goal              0..n
===================  ================  ===========


assumesPrecondition
###################
A precondition that must exist prior to the applied use of this design.




expressesAlgorithm
##################
An algorithm that defines the process used by the design in a generic way.




implentatedBy
#############
The design of the expressed algorithm is implemented by a Process




isDiscussedIn
#############
External materials that describe or discuss the design 




specifiesGoal
#############
The generic goal of a design.



