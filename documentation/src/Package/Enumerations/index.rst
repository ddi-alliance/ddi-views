************
Enumerations
************

A package is a administrative collection of classes in DDI.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   CategoryRelationCode/index.rst
   CollectionType/index.rst
   ComputationBaseList/index.rst
   MappingRelation/index.rst
   PointFormat/index.rst
   RelationSpecification/index.rst
   SexSpecificationType/index.rst
   ShapeCoded/index.rst
   SpatialObjectPairs/index.rst
   SpatialObjectType/index.rst
   SpatialRelationSpecification/index.rst
   StringStructureType/index.rst
   TableDirectionValues/index.rst
   TemporalRelationSpecification/index.rst
   TextDirectionValues/index.rst
   TotalityType/index.rst
   TrimValues/index.rst
   ValueRelationshipType/index.rst
   WhiteSpaceRule/index.rst



Graph
=====

.. graphviz:: /images/graph/Enumerations/Enumerations.dot