.. _fields:


BusinessFunction - Properties and Relationships
***********************************************





Properties
==========

========  =============================  ===========
Name      Type                           Cardinality
========  =============================  ===========
overview  InternationalStructuredString  0..1
========  =============================  ===========


overview
########
Provides a high level overview or summary of the class. Can be used to inform end-users or as part of an executive summary. Supports the use of multiple languages and structured text.

