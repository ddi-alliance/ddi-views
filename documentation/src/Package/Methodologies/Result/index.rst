.. _Result:


Result
******


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Describes the results of a process for the purpose of linking these results to guidance for future usage in specified situations. Result is abstract and serves as a substitution base for the specified result of a specific instantiation of a methodology process.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst