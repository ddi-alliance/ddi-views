.. _examples:


SampleFrame - Examples
**********************


A listing of all business enterprises by their primary office address with information on their industry classification, work staff size, and production costs. A telephone directory. An address list of housing units.