.. _fields:


SamplingAlgorithm - Properties and Relationships
************************************************





Properties
==========

=============================  ===========  ===========
Name                           Type         Cardinality
=============================  ===========  ===========
codifiedExpressionOfAlgorithm  CommandCode  0..1
=============================  ===========  ===========


codifiedExpressionOfAlgorithm
#############################
The algorithm expressed as algebraic or computer code.

