.. _SamplingDesign:


SamplingDesign
**************


Extends
=======
:ref:`DesignOverview`


Definition
==========
Sampling plan defines the how the sample is to be obtained through the description of methodology or a model based approach, defining the intended target population, stratification or split procedures and recommended sample frames. The content of a sampling plan is intended to be reusable. The application of the sample plan is captured in Sampling Process.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst