.. _fields:


SamplingProcedure - Properties and Relationships
************************************************





Relationships
=============

====================  =================  ===========
Name                  Type               Cardinality
====================  =================  ===========
componentMethodology  SamplingProcedure  0..n
hasDesign             SamplingDesign     0..n
hasProcess            SamplingProcess    0..n
isExpressedBy         SamplingAlgorithm  0..n
====================  =================  ===========


componentMethodology
####################
A Sampling Procedure that is a component part of this Sampling Procedure




hasDesign
#########
Sampling Design used to perform the Sampling Procedure




hasProcess
##########
Sampling Process used to perform the Sampling Procedure




isExpressedBy
#############
Sampling Algorithm that defines the methodology in a generic way



