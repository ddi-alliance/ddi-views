.. _examples:


SamplingProcess - Examples
**************************


A Split which takes a sampling stage and divides the sample frame into different subsets. A different sampling technique is applied to each subset. Once a split occurs each subset can have stages underneath, and the number of states under each split subset may differ. A Stage is the application of a single sampling algorithm applied to a sampling frame. For instance, the US Current Population Survey samples geographic areas first before identifying household to contact within each of those areas. A Stratification of a stage into multiple subsets. Each stratified group will be sampled using the same sampling approach. For example stratifying a state by ZIP Code areas in each of 5 mean income quintiles and then doing a random sample of the households in a set of Zip Codes. Allows for oversampling of identified subpopulations.