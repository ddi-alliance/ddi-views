.. _fields:


ProcessStep - Properties and Relationships
******************************************





Properties
==========

==================  =======  ===========
Name                Type     Cardinality
==================  =======  ===========
hasInformationFlow  Binding  0..n
==================  =======  ===========


hasInformationFlow
##################
Bindings used to pass data into and out of a Process Step.




Relationships
=============

==================  =========  ===========
Name                Type       Cardinality
==================  =========  ===========
hasInputParameter   Parameter  0..1
hasOutputParameter  Parameter  0..1
isPerformedBy       Service    0..n
==================  =========  ===========


hasInputParameter
#################
An identification for a Parameter related to the ProcessStep and referred to by a Binding as Input to the ProcessStep. 




hasOutputParameter
##################
An identification for a Parameter related to the ProcessStep and referred to by a Binding as Output from the ProcessStep. 




isPerformedBy
#############
Service performing the Process Step.



