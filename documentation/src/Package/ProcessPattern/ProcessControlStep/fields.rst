.. _fields:


ProcessControlStep - Properties and Relationships
*************************************************





Relationships
=============

========  ===========  ===========
Name      Type         Cardinality
========  ===========  ===========
executes  ProcessStep  0..n
========  ===========  ===========


executes
########
The ProcessSteps whose execution order is defined by the control structure.



