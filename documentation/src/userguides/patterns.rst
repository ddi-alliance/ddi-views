*************************************
Introduction to Patterns in DDI 4
*************************************

Introduction
============
DDI-Views introduces a new feature for those developing the model. There are “pattern” packages that consist entirely of abstract classes that can be “realized” by other classes. The realizing classes must mirror all of the properties and relationships of the pattern class although the properties of relationships may be renamed to make the semantics clearer. This document describes several of the pattern classes and their use.

Using the Collections pattern
=============================
DDI-Views introduces a generic Collections pattern that can be used to model different types of groupings, from simple unordered sets to all sorts of hierarchies, nesting and ordered sets/bags. 

.. image:: ../images/patterns_collection.png

A collection is a container, which could be either a set (i.e. with unique elements) or a bag (i.e. with repeated elements), of members. Collections can also be extended with richer semantics (e.g. generic, partitive, and instance, among others) to support a variety of DDI 3.x and GSIM structures, such as Node Sets, Schemes, Groups, sequences of Process Steps, etc. The Collections pattern classes, provide an abstraction to capture commonalities among a variety of seemingly disparate structures.

A SimpleCollection consists of zero, one or more CollectionMembers. A CollectionMember could potentially belong to multiple SimpleCollections. Membership in a SimpleCollection is maintained by contains properties of type MemberIndicator, one for each member in the collection. For sequentially ordered collections, the MemberIndication type includes an optional index property of type integer with the position of the member in the collection. For unordered collections, the index property is not included. 

A SimpleCollection is also a CollectionMember, which allows for nesting of collections in complex structures. CollectionMembers have to belong to some SimpleCollection, except in the case of nested collections where the top level is a member that doesn’t belong to any collection.
This pattern can be used via a special type of association called realizes. DDI-Views uses realizes to say that a class “behaves” like a SimpleCollection. For instance, consider a Set that consists of Elements, they implement the Collection pattern as follows: Set realizes SimpleCollection and Element realizes CollectionMember. 

To realize this pattern, all classes involved must be associated in a way that is compatible with the pattern. As a rule of thumb, a more restrictive type of association than the one that appears in the pattern is compatible, a looser one is not. For instance, since a SimpleCollection uses a contains property to link to its member (via a MemberIndicator type), classes realizing the pattern need to be related by property as well. In addition, the property has to be on the class realizing SimpleCollection, not on the class realizing CollectionMember. Similar compatibility rules apply to cardinality. Furthermore, all associations must be realized, with the exception of IsA associations, which are usually part of the pattern definition and do not apply to individual realizations in the same way. Renaming properties and associations does not affect compatibility as long as the documentation clearly explains how they map to the association in the pattern.

.. image:: ../images/patterns_compatible.png

For instance, consider the model diagram above. In this example, a Set class (on the left) is defined as being composed of at least one Element, i.e. no empty Sets are allowed (indicated by the cardinality on the property has). In addition, an Element always belong to one and only one Set, which means that deleting the Set will also delete its Elements (indicated by the cardinality on the association between ElementIndicator and Element). Such definition is compatible with the Collections pattern and thus Set, ElementIndicator and Element can realize SimpleCollection, MemberIndication and CollectionMember, respectively. In contrast, Schema and XML Instance (on the right) cannot realize the pattern for two reasons: (i) an association cannot realize a property, and (ii) Schema is not a grouping of XML Instances. Even if (i) is fixed by replacing the has association with a has property, such a property would have to be on the wrong class to maintain the “satisfies” semantics, i.e. on the member as opposed to the collection. The way it is defined, a Schema is not a collection of XML Instances. 

Beyond the sequential ordering provided SimpleCollection, the Collections pattern provides a StructuredCollection that support more complex structures and orderings of members via the RelationStructure and MemberRelation classes. 

A RelationStructure consists of one or more MemberRelations, which are tuples representing the relationship between one member (source) and possibly several members (target).

A RelationStructure can have different characteristics, e.g. totality, reflexivity, symmetry, and transitivity, all of which can be useful for reasoning. Totality is captured by the totality property (TotalityType datatype), and the others are captured by the hasRelationSpecification property (RelationSpecification datatype).

.. image:: ../images/patterns_classification.png

 A RelationStructure is said to be: 
* Total, if all members of the associated collection are related to each other (Otherwise, it is said to be partial). 
* Symmetric, if for any pair of members, a, b in the associated collection, whenever a is related to b then also b is related to a (Otherwise, it is said to be anti-symmetric). 
* Reflexive, if all members of the associated collection are related to themselves. (Otherwise, it is said to be anti-reflexive).
* Transitive, if for any members a, b, c in the associated collection, whenever a is related to b and b is related to c then a is also related to c. (Otherwise, it is said to be anti-transitive)

These characteristics can be combined to define different types of RelationStructures, e.g. equivalence relations and partial order relations, among others. Equivalence relations are useful to define partitions and equivalence classes (e.g. Levels in a Classification). Partial order relations can be used to represent lattices (e.g. class hierarchies, partitive relationships), parent-child relations can define trees and acyclic precedence relations can represent directed acyclic graphs (e.g. molecular interactions, geospatial relationships between regions). 

Someone creating instances can use the different characteristics as a guide when trying to decide what type of StructuredCollection to instantiate, e.g., if the new instance to be created is a Statistical Classification containing ClassificationItems that will be organized into a parent-child hierarchy, then the associated CodeRelationStructure will have hasRelationSpecification = “ParentChild”. 
Let us illustrate how this model works with a simple instance. Consider a North American Industry Classification System (NAICS) StatisticalClassification with ClassificationItems representing type of economic activity, such as Mining, Manufacturing, Finance, etc. 

.. image:: ../images/patterns_statisticalclassification.png

As you can see from the diagram above, StatisticalClassification is a realization of StructuredCollection and ClassificationItem is an indirect realization of CollectionMember, i.e. via Designation and Sign. This means we can view ClassificationItems such as Manufacturing, Machinery manufacturing, Educational services, etc. as CollectionMembers organized in a hierarchy by a ClassificationItemRelationStructure which consists of a set of ClassificationItemRelations representing the parent-child relationships between items. For instance, (Manufacturing, Machinery manufacturing) is a ClassificationItemRelation in which Manufacturing is the parent and Machine manufacturing is the child. 

Note that by maintaining the hierarchy in a separate structure, i.e. ClassificationItemRelationStructure, items can be reused in multiple classifications. For instance, a NAICS variant groups economic activities into two main industry groupings: the goods-producing industries and the services-producing industries. Because of the separation of hierarchy and categories, adding that high-level grouping doesn’t require a change in the structure and definition of the underlying industry ClassificationItems.

StatisticalClassifications sometimes need to be mapped to each other. In our NAICS example, we may want to relate the variant above with another variant including only ClassificationItems about the information and communication technology (ICT) sector. We use a CorrespondenceTable to link both variants and correspondence properties to link their ClassificationItems. The Map datatype in the correspondence property includes a CorrespondenceType to describe the type of relationship that exists between the related ClassificationItems, in terms of equivalency, commonalities, differences, etc. For instance, the ICT-Manufacturing sector (code 31-33i) in the ICT variant can be mapped to the Manufacturing sector (code 31-33) whereas the CorrespondenceType is “inclusion” since the former is a subset of the latter.  

Using the Process pattern 
=====================
Another pattern introduced in DDI-Views is Process. It consists of a basic set of classes to describe process steps and information flows between them. Some of its classes are extensions of classes in the Collections Pattern. The diagram on the left shows the Process pattern.

.. image:: ../images/patterns_process.png

A Process may contain a single top-level ProcessStep. A ProcessControlStep handles the execution flow of the ProcessSteps in its scope. A ProcessStep performs one or more business functions at any granularity. Each ProcessStep can be performed by a Service. ProcessSteps can be nested, via a ProcessControlStep, and thus describe processes at multiple levels of detail. Information flows specify how information objects move between ProcessSteps by mapping parameters as sources or targets. 
The Process Pattern can be realized in multiple ways. DDI-Views include a well-known realization called Workflow, which can be mapped to process execution languages like BPEL or BPMN. 

A WorkflowProcess is a Collection of WorkflowSteps that perform one or more business functions. Each WorkflowStep can be performed by a WorkflowService. There are two types of WorkflowSteps: Acts, and WorkflowControlSteps. Acts represent actions and are atomic, i.e. they cannot be composed of other WorkflowSteps. An Act is similar to a terminal in the production rules of a formal grammar and an instruction in a programming language. An Act can be either a MetadataDrivenAction or a ComputationAction. MetadataDrivenActions are defined by validation and transformation rules expressed in some standard rule language whereas ComputationAction can perform computation in more generic programming languages. 

A ConditionalControlStep describes logical execution flows between Workflow Steps. ConditionalControlSteps can be nested via a WorkflowStepSequence and thus describe workflows at multiple levels of detail. The nesting of WorkflowSteps always terminates in an Act. A WorkflowStepSequence can be ordered using the orderedStep property of type WorkflowStepOrder. The WorkflowStepSequence at the end of the executes association represents the body of the ConditionalControlStep, which is executed depending on the result of the condition evaluation. The specialized sub-classes determine whether the steps in the body are executed in each iteration before the condition is evaluated (RepeatUntil), or after (RepeatWhile, Loop, IfThenElse). The Loop also provides a counter with initialValue and stepValue that can be used to specify in the condition how many times the steps in the body are executed. 

IfThenElse provides a means to specify branching control flows. In addition to the executes already mentioned,  there is an elseExecutes pointing to an optional WorkflowStepSequence to be executed if the condition is evaluated to false. Optionally, IfThenElse can also have a property ElseIf to model switch statements.

.. image:: ../images/patterns_workflow.png

TemporalRelationControlSteps provide a mechanism for expressing concurrency control and synchronization, via the Split and SplitJoin subclasses, and for capturing Allen’s interval relations, one of the best-established formalisms for temporal reasoning, via the TemporalRelationSpecification. Temporal Interval Relations can be used to define temporal constraints between pairs of WorkflowSteps, e.g. whether the execution of two WorkflowSteps can overlap in time or not, or one has to finish before the other one starts, etc. Note that this also supports parallel processing.
There are thirteen Temporal Interval Relations: twelve asymmetric ones, i.e. precedes, meets, overlaps, finishes, contains, starts and their converses, plus equals, which is the only one that has no converse or, rather, it is the same as its converse. Together these relations are distinct (any pair of definite intervals are described by one and only one of the relations), exhaustive (any pair of definite intervals are described by one of the relations), and qualitative (no numeric time spans are considered).
Following Allen’s, Temporal Interval Relations are defined as follows.

.. image:: ../images/patterns_time.png

Let’s see how Temporal Interval Relations work with an example.

Consider a questionnaire with a set of questions that need to satisfy the following flow conditions: (i) question Q3 requires the answer of both Q1 and Q2, and (ii) question Q4 is triggered by answering question Q2. Note the difference between requiring an answer and being triggered by an answer. The former means that there is no necessary immediate execution, i.e. Q3 can be executed long after both Q1 and Q2 have been answered. In other words, the exact moment for executing Q3 will depend on other parts of the control flow logic of the questionnaire, i.e. other constraints defined in the questionnaire flow. However, when a question triggers another it means that the latter is executed right after the former is answered. 

With that in mind, the precedence constraint between Q1, Q2 and Q3 can be modeled with a TemporalRelationControlStep of type “precedes” whereas the one between Q2 and Q4 can be represented with a TemporalRelationControlStep of type “meets”. Remember that the fact that Q1 precedes Q3 means that Q1 finishes before Q3 starts whereas Q2 meets Q4 means that Q4 starts exactly when Q2 finishes, which is exactly what the two TemporalRelationControlSteps mentioned above can express.

So far, we have described the control flow logic. How does information flow between the steps in the control flow logic? There is a property in ProcessStep (and therefore in WorkflowStep) for that purpose, i.e. hasInformationFlow, of Binding datatype. A Binding is a design-time class that maps Parameters between WorkflowSteps to represent how information flows between them. 

.. image:: ../images/patterns_binding.png

Let’s illustrate the use of Bindings with an example.

Consider a Workflow that implements a fragment of the GSBPM Process phase. This is a specific implementation of GSBPM within the context of an organization in which some sub-steps are not implemented.

.. image:: ../images/patterns_gsbpm.png

The step at the top, Process Data, has four Parameters, three Input and one Output, each with its type between brackets, i.e. Classification, Instance Variable and Edit Rules for Input and Instance Variable for Output. This means that each parameter can hold at runtime only objects of the specified type. 
There are also two sub-steps organized in a Workflow Sequence. The first sub-step, Code, has an Instance Variable and a Classification as Inputs and an Instance Variable as Output. The second sub-step, Edit and Impute, has the coded Instance Variable and Edit Rules as Input and an Instance Variable as Output.

How do we put all the pieces together? The top step gets a collected Instance Variable and produces a coded, edited and imputed Instance Variable based on a Classification and some Edit Rules. This is achieved by invoking the two sub-steps just described in sequence.

In order for this to work, the input and output Parameters of each step have to map so that the Instance Variable the top step receives moves thru the two sub-steps and is returned back, transformed, to the top step. This is made possible by the Bindings. 

The first Binding on the left maps the input Classification Parameter of Process Variable to that of the Code sub-step. This is an example of input-to-input Binding between two levels, i.e. a container step and its sub-steps. The bottom Binding maps the output Instance Variable Parameter of Code to the input Instance Variable Parameter of Edit and Impute. This is an example of an output-to-input Binding between steps at the same level, i.e. within the same container. The other Bindings make sure that all Parameters are mapped.
 
Using the Signification pattern
=========================
*Based on “Identifiers, Labels, Names, and Designations”, by Frank France and Dan Gillman*

A Sign links a Signified with a Signifier that denotes it. A Signifier is a concept whose extension consists of tokens (perceivable objects). 

Signifier, Sign and Signified become part of the Signification Pattern.

.. image:: ../images/patterns_signification.png

A Designation is simply a Sign where the Signified is a Concept. Therefore Designation and Sign realize Sign and Signified, respectively. Signifier becomes the Data Type of the representation property of Sign. 
The reason for making Signifier, Sign and Signified into a pattern to be realized as opposed to classes to be extended is that Concepts are not always Signifieds, which is what a specialization would imply. In fact, a Concept is a Signified only if there is a Designation that denotes it. The realization means that the Concept is going to behave like a Signified only in the context described.

Codes enter into the picture as Designations. A Code then is a type of Designation that has Non-Linguistic Signifiers and where the Signified is a Category (Concept). 
