********************************
Restrictions in Functional Views
********************************

The XML binding incorporates restrictions on relations between classes in the XML schema that corresponds to each view. Currently, there is not a comparable way of enforcing restrictions with RDF instances. Instead, RDF instance builders need to be mindful of the restrictions specified here. In the future, ShEx and/or SHACL descriptions may be provided to support RDF instance validation.

Restrictions are used to limit the coverage of the Functional View.

Restrictions are made by not including one or more target classes in a relationship. All restrictions should be noted in the documentation of the Functional View. They are not noted at the class level.

Restrictions are used to limit the coverage of the Functional View at points where:

* **Sections of the DDI model relate to each other**
* Example:  In Data Description View an InstanceVariable has a relationship to a sourceCapture where the target is the abstract class Capture which has the subtypes QuestionIntance or MeasureInstance. Neither of these classes are included.
* Example: The Data Management View uses a WorkflowStepSequence which contains a WorkflowStepIndicator with a relationship of member with the target class WorkflowStep. This abstract class has a large number of subtypes many of which are related to specific applications of the Workflows structure. The list below shows all subtypes of WorkflowStep and indicates whether or not they are included in the Data Management View. Classes not included would be listed as restrictions in whatever class contains a relationship with the target class of WorkflowStep.


.. image:: ../images/restrictions_table.png
    :width: 200px
    :align: center

* Locations where extended detail is not required
     * Example: In the Data Management View the class Study has the relationship hasMethodology with the target class MethodologyOverview. MethodologyOverview has a subtype SamplingProcedure which is not included in the view. Study was included through the isInStudy relationship from DataPipeline, but the intended coverage of the view does not extend to a complete description of a Study.
* Locations where not all subtypes are relevant
     * Example: If the sole use of the target Agent in a Functional View was by FundingInformation or by other locations where Machine was not an appropriate subtype. All relationships with the target Agent would be restricted to Organization and Individual.

For a complete breakdown of restricted classes by View, please see the PrototypeViews-2018-09-10.xlsx spreadsheet included in the prototype package.