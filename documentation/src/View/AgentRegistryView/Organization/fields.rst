.. _fields:


Organization - Properties and Relationships
*******************************************





Properties
==========

=====================  ==================  ===========
Name                   Type                Cardinality
=====================  ==================  ===========
ddiId                  String              0..n
hasContactInformation  ContactInformation  0..1
hasOrganizationName    OrganizationName    1..n
=====================  ==================  ===========


ddiId
#####
The agency identifier of the organization as registered at the DDI Alliance register.


hasContactInformation
#####################
Contact information for the organization including location specification, address, URL, phone numbers, and other means of communication access. Sets of information can be repeated and date-stamped.


hasOrganizationName
###################
Names by which the organization is known.

