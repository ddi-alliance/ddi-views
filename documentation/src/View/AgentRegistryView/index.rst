*****************
AgentRegistryView
*****************
Purpose:

The Agent Registry View supports the creation of a listing of Agents (Organization, Individual, Machine) for the purpose of maintenance and reuse. The listing of Agents can be organized as simple lists or in more complicated structures through the use of an AgentRelationStructure. These may be typed and provided with effective date ranges either for the whole AgentRelationStructure or for individual AgentRelations. Complex relationships may be described among Individuals or among Organizations or among Machines. Relationships between members of these classes may also be described.

Use Cases:

Maintaining a listing of all Organizations and Individuals related to a project or statistical activities, i.e. Individuals and Organizations related to the IPUMS projects at the Minnesota Population Center or the Departments and Individuals involved in the production of the Australian Census within the Australian Bureau of Statistics

Organizational affiliations of Individuals

Collaboration networks or other social networks


Target Audience:

Organizations working with a number of Individuals and Organizations whose descriptive information and interrelationships change over time.

Restricted Classes:

Concept is extended by Category, ConceptualVariable, RepresentedVariable, InstanceVariable, Population, UnitType and Universe

ExternalMaterial is extended by ExternalAid

General Documentation:

The primary class is the Agent List which can be structured by AgentRelationStructure. Agents may be of any type supported by DDI at the time of this release. Agent relationships can be defined using the adjacency list Agent Relation within AgentRelationStructure. specifying the RelationshipSpecification (Equality, Parent/Child, Whole/Part, etc.), a relevant semantic such as a specific role or relationship type, and effective time span for the relationship.

A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   AgentListing/index.rst
   AgentRelationStructure/index.rst
   Concept/index.rst
   ExternalMaterial/index.rst
   Individual/index.rst
   Machine/index.rst
   Organization/index.rst



Graph
=====

.. graphviz:: /images/graph/AgentRegistryView/AgentRegistryView.dot