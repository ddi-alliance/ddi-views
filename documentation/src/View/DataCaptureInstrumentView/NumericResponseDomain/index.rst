.. _NumericResponseDomain:


NumericResponseDomain
*********************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a numeric response (the intent is to analyze the response as a number) for a question.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst