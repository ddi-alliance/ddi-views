.. _CategoryRelationStructure:


CategoryRelationStructure
*************************


Extends
=======
:ref:`Identifiable`


Definition
==========
Relation structure of categories within a collection. Allows for the specification of complex relationships among categories.


Synonyms
========



Explanatory notes
=================
The CategoryRelationStructure employs a set of CategoryRelations to describe the relationship among concepts. Each CategoryRelation is a one to many description of connections between categories. Together they might commonly describe relationships as complex as a hierarchy. This is a kind of a ConceptRelationStructure restricted to categories (which are concepts).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst