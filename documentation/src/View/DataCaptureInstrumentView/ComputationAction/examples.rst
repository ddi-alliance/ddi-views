.. _examples:


ComputationAction - Examples
****************************


In data processing, a ComputationAction might be a code statement in a code sequence. In data capture, a ComputationAction might takes the form of a question, a measurement or instrument code.