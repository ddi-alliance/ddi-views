.. _RankingResponseDomain:


RankingResponseDomain
*********************


Extends
=======
:ref:`ResponseDomain`


Definition
==========
A response domain capturing a ranking response which supports a "ranking" or "Ordering" of provided categories. Note: This item still must be modeled and is incomplete at this time.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst