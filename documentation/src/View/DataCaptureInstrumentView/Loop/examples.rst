.. _examples:


Loop - Examples
***************


A loop is set to repeat a fixed number of times (initialValue). A stepValue is initially specified. At the end of the loop the current value is decremented by the stepValue. The current value might be the initialValue or it might be the current value after the initialValue has been decremented one or more times.