*************************
DataCaptureInstrumentView
*************************
Purpose:

The purpose of the DDI4 Data Capture Instrument View is to describe the processes of developing- and collect data from a questionnaire instrument, as well as to describe the capture or acquiring of measurements from various sources, like databases, registries, administrative data, bio-medical devices, environmental sensors, or any other source or instrument. The focus on measurement is new in DDI4 compared to earlier versions of the standard.


Use Cases:

Document  the development process for a conceptual questionnaire of an international survey

Design an implemented questionnaire in a specific mode and for a specific population

View how a measurement has been captured or how a survey question has been asked

Reuse of instrument components developed for a different study

Export instrument related metadata between applications

Design a measurement

Describe a measurement  instrument



Target Audiences:

Developers of conceptual instruments, for example for an international survey.

Designers of implemented instruments for surveys and measurements.

Designers and developers of data capture tools.

Researchers wishing to understand how a measurement has been captured or how a survey question was asked or how it was developed.

DDI4 users in general.


Restricted Classes:

WorkflowStepSequence is extended by StructuredWorkflowSteps.

RepresentedVariable is extended by InstanceVariable.

CodeList is extended by GeographicUnitClassification, GeographicUnitTypeClassification and StatisticalClassification.

ConceptualVariable is extended by InstanceVariable

Concept is extended by InstanceVariable, Population and UnitType;

ExternalMaterial is extended by ExternalAid.

Code is extended by ClassificationItem.


General Documentation:

Questionnaires:

The goal of questionnaire design is to develop an ImplementedInstrument from which data can be captured when put in field. The first step in a questionnaire design process is often to select or define the topics and the Concepts to be measured by the questions in the instrument. The concepts are then operationalized into RepresentedQuestions that contains the more reusable components of a question that can be reused among different surveys, studies and modes of collection. InstanceQuestions realize the represented questions in an instrument. It connects to other InstrumentComponents like for example a showcard or other ExternalAid and Instructions, and links the question to the flow logic of the questionnaire. The ConceptualInstrument represents the instrument independent of mode. It organizes the instrument by connecting the instrument components to WorkflowStepSequence that realizes the flow of a questionnaire sequence, including conditions IfThenElse, Loop etc. The ImplementedInstrument has a specific mode and is designed or adapted for a specific sample or population. Two different implemented instruments may use the same conceptual instrument. 

Measurements:

Measurements are non-question based data captures like for example blood pressure measurements, MRI images, thermometer, web-service, and experimental observations. Measurements often involve the use of specific machines or protocols to ensure consistency and comparability of the measure. The RepresentedMeasurement is a description of the components of a measurement, that can be reused by multiple or repeated studies. The ImplementedMeasurement initiates the represented measurement so that it can be used in the data capture process. Other InstrumentComponents like Instructions and ExternalAids associated with the measurement can be linked to the implemented instrument.
The ConceptualInstrument represents the reusable components of the instrument that can have different concrete implementations. It connects to the WorkflowStepSequence that realizes the instrument flow. A WorkflowStep can be linked to a WorkflowService that involves agents and services involved in the process step. The ImplementedInstrument is a specific implementation of a conceptual instrument.


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   BooleanResponseDomain/index.rst
   Category/index.rst
   CategoryRelationStructure/index.rst
   CategorySet/index.rst
   Code/index.rst
   CodeList/index.rst
   CodeListResponseDomain/index.rst
   CodeRelationStructure/index.rst
   ComputationAction/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   ConceptualInstrument/index.rst
   ConceptualVariable/index.rst
   ExternalAid/index.rst
   ExternalMaterial/index.rst
   IfThenElse/index.rst
   ImplementedInstrument/index.rst
   InstanceMeasurement/index.rst
   InstanceQuestion/index.rst
   Instruction/index.rst
   InstrumentCode/index.rst
   LevelStructure/index.rst
   Loop/index.rst
   NumericResponseDomain/index.rst
   Parameter/index.rst
   RankingResponseDomain/index.rst
   RepeatUntil/index.rst
   RepeatWhile/index.rst
   RepresentedMeasurement/index.rst
   RepresentedQuestion/index.rst
   RepresentedVariable/index.rst
   ScaleResponseDomain/index.rst
   SentinelValueDomain/index.rst
   Statement/index.rst
   SubstantiveValueDomain/index.rst
   TextResponseDomain/index.rst
   UnitType/index.rst
   Universe/index.rst
   WorkflowStepSequence/index.rst



Graph
=====

.. graphviz:: /images/graph/DataCaptureInstrumentView/DataCaptureInstrumentView.dot