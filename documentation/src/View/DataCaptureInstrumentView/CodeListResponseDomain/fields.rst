.. _fields:


CodeListResponseDomain - Properties and Relationships
*****************************************************





Relationships
=============

=============================  ==============  ===========  ================
Name                           Type            Cardinality  allways external
=============================  ==============  ===========  ================
specifyOtherCorrespondingCode  Code            0..n           no
specifyOtherResponseDomain     ResponseDomain  0..n           no
usesCodeList                   CodeList        0..n           no
=============================  ==============  ===========  ================


specifyOtherCorrespondingCode
#############################
Optionally allows specifying the Code to which a secondary response domain is attached. For example, link to code which signifies "Other" within this CodeList.




specifyOtherResponseDomain
##########################
Optionally allows specifying the attached secondary response domain used by a specific item within this CodeListResponseDomain. For example, attach a TextResponseDomain to the code for "Other" using the TextDomain label (Please specify) as a label for the TextDomain. The attached secondary response domain must also be added to the Represented Question or Measure.




usesCodeList
############
The CodeList which includes the response options to list.




