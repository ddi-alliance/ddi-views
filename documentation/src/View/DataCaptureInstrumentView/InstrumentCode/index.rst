.. _InstrumentCode:


InstrumentCode
**************


Extends
=======
:ref:`InstrumentComponent`


Definition
==========
An InstrumentComponent that specifies the performance of a specific computation within the context of an instrument flow.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst