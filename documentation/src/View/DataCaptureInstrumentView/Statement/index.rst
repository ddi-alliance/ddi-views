.. _Statement:


Statement
*********


Extends
=======
:ref:`InstrumentComponent`


Definition
==========
A Statement is a type of Instrument Component containing human readable text or referred material.


Synonyms
========
DDI:StatementItem


Explanatory notes
=================
It is not directly related to another specific Instrument Component such as an InstanceQueston or InstanceMeasurement. It may be placed anywhere in a WorkflowStepSequence.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst