.. _ConceptSystemCorrespondence:


ConceptSystemCorrespondence
***************************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Relationship between 2 or more ConceptSystems described through mapping similarity relationships between their member Concepts.


Synonyms
========



Explanatory notes
=================
Describes correspondence with one or more Maps which identify a source and target concept and defines their commonality and difference using descriptive text and controlled vocabularies.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst