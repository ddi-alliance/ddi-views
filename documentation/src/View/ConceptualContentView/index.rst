*********************
ConceptualContentView
*********************
Purpose:

To review the conceptual concept structures and to manage concept systems (other than statistical classifications). The coverage goes down to a RepresentedVariable. InstanceVariable has been included in the collection for use as a member of a VariableCollection. However it does not go into the detailed content of an InstanceVariable.

Restriction: 

InstanceVariable has been included for the sole purpose of membership in a VariableCollection. The classes required to completely describe an InstanceVariable have not been included.


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   Category/index.rst
   CategoryRelationStructure/index.rst
   CategorySet/index.rst
   Code/index.rst
   CodeList/index.rst
   CodeRelationStructure/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   ConceptSystemCorrespondence/index.rst
   ConceptualVariable/index.rst
   ExternalMaterial/index.rst
   Individual/index.rst
   LevelStructure/index.rst
   Organization/index.rst
   Population/index.rst
   RepresentedVariable/index.rst
   SentinelConceptualDomain/index.rst
   SentinelValueDomain/index.rst
   SubstantiveConceptualDomain/index.rst
   SubstantiveValueDomain/index.rst
   Unit/index.rst
   UnitType/index.rst
   Universe/index.rst
   ValueAndConceptDescription/index.rst
   VariableCollection/index.rst
   VariableRelationStructure/index.rst



Graph
=====

.. graphviz:: /images/graph/ConceptualContentView/ConceptualContentView.dot