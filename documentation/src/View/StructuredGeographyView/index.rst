***********************
StructuredGeographyView
***********************
Purpose:

The purpose of Geographic Classification in DDI is to express relationships over time and space that are currently only available in geographic shape files or by exploring the data file itself. A great deal of legacy data does not have related geographic files. A large group of areas covered by DDI do not have clearly defined geography in terms of spatial extent, but have considerable relational information. Many geographic structures define geographic unit types with descriptive sub-sets (e.g. “Places of over 10,000 population” where the user must know whether a specific location meets that definition prior to determining if it is included in the data set).

Use Cases:

Describing geographic structures and their hierarchies.

Describing geographic units and their corresponding locations (polygons, bounding boxes, points etc.).

Documenting changes to geographic units over time.


Target Audiences:
Digital archivists

Search engines intent on exposing data by their geographical coverage

Researchers who need to describe complex geographical structures

Researchers who need to describe geographical structures and locations with changes over time

Restricted Classes:
ConceptSystem is extended by CategorySet.
 
Concept is extended by Category, ConceptualVariable, RepresentedVariable, InstanceVariable, Population and Universe. 

ExternalMaterial is extended by ExternalAid.

General Documentation:

Geographic classification uses unit, unit type, and collections to provide the basis for describing the geographic structure (unit types) and their individual locations (units) in a manner that supports complex layered hierarchies, changes in name, role, and geographic extent over time, and coding for direct use in value domains and responses. It supports the content of DDI Lifecycle Geographic Structure and Geographic Location.

The revision of the DDI 3.2 structure into the DDI4 structure addresses the following constraints in DDI 3.2:

Codes and names can be used directly wherever a code list can be used (both Geographic Unit Type Classification and Geographic Unit Classification are sub-types of Code List.

A Geographic Unit (location) can belong to multiple Geographic Unit Type (geographic levels) with replicating its content.

A Geographic Unit can version and/or define new units whenever there is a change in name or spatial extent as determined by the user. For example a Metropolitan Area may change only its spatial extent over time and be managed as versions of the same conceptual location; a city may annex additional land, or be renamed. Normally a new version of a Geographic Unit should have some common spatial extent with the old version. This can be described in the Geographic Unit precedes or supersedes using the Spatial Relationship using Spatial Relation Specification (Equals | Disjoint | Intersects | Contains | Touches).

Geographic Units can be used to describe points, lines, and/or polygons as well as include centroid information.

Equivalent Content between DDI 3.2 and DDI 4

(DDI 3.2 - DDI 4 - Comment)

Geographic Structure - Geographic Unit Type Classification - no comment

Geographic Level - Unit Type - Because no specialized information was being gathered a generic Unit Type is used. This allows users to declare any Unit Type as part of geography without requiring that it be of a specific type.

Parent Structure / Geographic Layer Base - Geographic Unit Type Relation Structure - Described as separate hierarchies; can have levels; Spatial relationships can be described

Geographic Level Code - Geographic Unit Type Instance - Codes are assigned when the Unit Type is used by a Geographic Unit Type Classification (see information on contains in Geographic Location)
Primary Component - Geographic Unit Type Relation Structure - Spatial relationships can be described

Geographic Location - Geographic Unit Classification - Inclusion of a Geographic Unit using “contains” allows for the assignment of a code in the same manner as in a code list. Different code sets should be expressed as different geographic unit classifications. Inclusion is by reference so that the described Geographic Unit can be assigned multiple codes without altering the Geographic Unit description

Location Value - Geographic Unit - Rather than grouping with other units of the same unit type, each unit can relate itself to one or more unit types. This allows for a location like Washington, D.C. to act as a State equivalent, County equivalent, and Place in the United States. Provides supersedes and precedes.


A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   BoundingBox/index.rst
   Concept/index.rst
   ConceptRelationStructure/index.rst
   ConceptSystem/index.rst
   ExternalMaterial/index.rst
   GeographicExtent/index.rst
   GeographicUnit/index.rst
   GeographicUnitClassification/index.rst
   GeographicUnitRelationStructure/index.rst
   GeographicUnitTypeClassification/index.rst
   GeographicUnitTypeRelationStructure/index.rst
   Individual/index.rst
   LevelStructure/index.rst
   Organization/index.rst
   UnitType/index.rst



Graph
=====

.. graphviz:: /images/graph/StructuredGeographyView/StructuredGeographyView.dot