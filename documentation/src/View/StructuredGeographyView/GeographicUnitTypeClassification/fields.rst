.. _fields:


GeographicUnitTypeClassification - Properties and Relationships
***************************************************************





Properties
==========

============  =============================  ===========
Name          Type                           Cardinality
============  =============================  ===========
contains      GeographicUnitTypeIndicator    0..n
displayLabel  LabelForDisplay                0..n
isCurrent     Boolean                        0..1
isFloating    Boolean                        0..1
releaseDate   Date                           0..1
usage         InternationalStructuredString  0..1
validDates    DateRange                      0..1
============  =============================  ===========


contains
########
Define the code and Unit Type that is a member of the Geographic Unit Type Classification. They may be unordered or ordered and assigned to a specific level.


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


isCurrent
#########
Indicates if the Statistical Classification is currently valid.


isFloating
##########
Indicates if the Classification is a floating classification. In a floating statistical classification, a validity period should be defined for all Classification Items which will allow the display of the item structure and content at different points of time. (Source: GSIM StatisticalClassification/Floating)


releaseDate
###########
Date the Classification was released


usage
#####
Explanation of the ways in which some decision or object is employed. Supports the use of multiple languages and structured text.


validDates
##########
The date the statistical classification enters production use and the date on which the Classification was superseded by a successor version or otherwise ceased to be valid. (Source: GSIM Statistical Classification)




Relationships
=============

==============  ===================================  ===========  ================
Name            Type                                 Cardinality  allways external
==============  ===================================  ===========  ================
isMaintainedBy  Organization                         0..n           no
isReplacedBy    GeographicUnitTypeClassification     0..n           no
isStructuredBy  GeographicUnitTypeRelationStructure  0..n           no
realizes        StructuredCollection                 0..n           yes
references      ConceptSystem                        0..n           no
replaces        GeographicUnitTypeClassification     0..n           no
variantOf       GeographicUnitTypeClassification     0..n           no
==============  ===================================  ===========  ================


isMaintainedBy
##############
Organization, agency, or group within an agency responsible for the maintenance and upkeep of the classification.




isReplacedBy
############
Geographic Unit Type Structure that supersedes the actual Classification (for those Classifications that are versions or updates),




isStructuredBy
##############
A relation structure used to describe complex relationships between the members




realizes
########
Collection class realized by this class




references
##########
May reference an existing concept system generally of Geographic Unit Types




replaces
########
Geographic Unit Type Structure superseded by the actual Classification (for those Classifications that are versions or updates).




variantOf
#########
Geographic Unit Type Structure on which the current variant is based, and any subsequent versions of that Classification to which it is also applicable. 



