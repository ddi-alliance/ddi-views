.. _fields:


MetadataDrivenAction - Properties and Relationships
***************************************************





Properties
==========

==========================  =================================  ===========
Name                        Type                               Cardinality
==========================  =================================  ===========
activityDescription         InternationalStructuredString      0..1
quasiVTL                    InternationalStructuredString      0..1
typeOfMetadataDrivenAction  ExternalControlledVocabularyEntry  0..1
==========================  =================================  ===========


activityDescription
###################
Describes the transformation activity


quasiVTL
########
A definition of the action in XHTML that conforms to the quasi-VTL schema. Each action has its own definition that a user optionally completes.


typeOfMetadataDrivenAction
##########################
Allows for the specification of a transformation type supporting the use of an external controlled vocabulary

