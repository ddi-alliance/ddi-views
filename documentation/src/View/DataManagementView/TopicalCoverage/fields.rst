.. _fields:


TopicalCoverage - Properties and Relationships
**********************************************





Properties
==========

=======  =================================  ===========
Name     Type                               Cardinality
=======  =================================  ===========
keyword  ExternalControlledVocabularyEntry  0..n
subject  ExternalControlledVocabularyEntry  0..n
=======  =================================  ===========


keyword
#######
A keyword that describes the topical coverage of the content of the annotated object.  Keywords may be structured (e.g. TheSoz thesauri) or unstructured and reflect the terminology found in the document and other related (broader or similar) terms.  Uses and InternationalCodeValue and may indicate the language of the code used.


subject
#######
A subject that describes the topical coverage of the content of the annotated object. Subjects are members of structured classification systems such as formal subject headings in libraries. Uses and InternationalCodeValue and may indicate the language of the code used. 

