.. _fields:


UnitDataRecord - Properties and Relationships
*********************************************





Relationships
=============

==============  =================================  ===========  ================
Name            Type                               Cardinality  allways external
==============  =================================  ===========  ================
isStructuredBy  InstanceVariableRelationStructure  0..n           no
isViewedFrom    UnitDataViewpoint                  0..n           yes
==============  =================================  ===========  ================


isStructuredBy
##############
Describe the order of InstanceVariables in a record for those cases where the variables have a more complicated logical order than a simple sequence.




isViewedFrom
############
A UnitDataRecord may have multiple ViewPoints, each of which assigns among roles identifier, measure, and attribute to the InstanceVariables associated with each Datapoint



