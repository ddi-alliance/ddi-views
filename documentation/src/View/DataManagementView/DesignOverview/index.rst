.. _DesignOverview:


DesignOverview
**************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
High level, descriptive, human informative, design statement The design may be used to specify how a process will be performed in general. This would most commonly be used in a Codebook along with an AlgorithmOverview and a MethodologyOverview. The design informs a specific or implemented process as to its general parameters. Supports specification of any realization of Goal.


Synonyms
========



Explanatory notes
=================
Allows for the use of any realized Process. The methodology, design, and algorithm of a specific realized process should be used if available. The use of a generic Process such as a WorkflowProcess containing an Act would be appropriate here. Restriction would be done by inclusion of the appropriate realized process class(es) in a Functional View.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst