.. _Split:


Split
*****


Extends
=======
:ref:`TemporalRelationControlStep`


Definition
==========
The components of a Split consists of a number of process steps to be executed concurrently with partial synchronization. Split completes as soon as all of its component process steps have been scheduled for execution.


Synonyms
========



Explanatory notes
=================
Supports parallel processing that does not require completion to exit.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst