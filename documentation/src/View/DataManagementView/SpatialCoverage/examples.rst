.. _examples:


SpatialCoverage - Examples
**************************


A country, a neighborhood, the inside of a polygon on the surface of the earth, along a street, at a particular intersection, or perhaps even in a certain orbit around the planet Mars. For a data set within a study this may be used to define the geographic restriction of the data set within the geographic coverage of the study (eg. The study may cover all of Sweden but the spatial coverage of the data set is Stockholm).