.. _examples:


SubstantiveValueDomain - Examples
*********************************


All real decimal numbers relating to the subject matter of interest between 0 and 1 specified in Arabic numerals. [GSIM 1.1]. The codes "M" male and "F" for female .