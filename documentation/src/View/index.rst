=====
Views
=====
			
.. image:: ../../_static/images/ddi-logo.*

.. warning::
  this is a development build, not a final product.

.. toctree::
   :caption: Table of contents
   :maxdepth: 2

   DescriptiveCodebookView/index.rst
   SamplingView/index.rst
   StatisticalClassificationView/index.rst
   StructuredGeographyView/index.rst
   DataManagementView/index.rst
   DataDescriptionView/index.rst
   ConceptualContentView/index.rst
   ControlledVocabularyView/index.rst
   CustomMetadataView/index.rst
   DataCaptureInstrumentView/index.rst
   AgentRegistryView/index.rst
