.. _fields:


CustomInstance - Properties and Relationships
*********************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
contains   CustomValueIndicator           0..n
isOrdered  Boolean                        0..1
name       ObjectName                     0..n
purpose    InternationalStructuredString  0..1
type       CollectionType                 0..1
=========  =============================  ===========


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array 


isOrdered
#########
If members are ordered set to true, if unordered set to false. 


name
####
see Collection


purpose
#######
see Collection


type
####
see Collection




Relationships
=============

===============  ================  ===========  ================
Name             Type              Cardinality  allways external
===============  ================  ===========  ================
correspondsTo    CustomStructure   0..n           no
definingConcept  Concept           0..n           no
realizes         SimpleCollection  0..n           yes
===============  ================  ===========  ================


correspondsTo
#############
Is realted to a CustomStructure




definingConcept
###############
The conceptual basis for the collection of members.




realizes
########
Collection class realized by this class



