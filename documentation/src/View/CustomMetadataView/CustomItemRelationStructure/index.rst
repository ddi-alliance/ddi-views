.. _CustomItemRelationStructure:


CustomItemRelationStructure
***************************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Contains a set of CustomItemRelations which together define the relationships of Custom Structure.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst