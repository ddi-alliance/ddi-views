.. _examples:


ClassificationFamily - Examples
*******************************


A family of industrial classifications each a separate series (i.e. U.S. Standard Industrial Classification (SIC) and North American Industrial Classification System (NAICS)