.. _Organization:


Organization
************


Extends
=======
:ref:`Agent`


Definition
==========
A framework of authority designated to act toward some purpose.


Synonyms
========



Explanatory notes
=================
related to org:Organization which is described as "Represents a collection of people organized together into a community or other..."


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst