.. _ClassificationItemRelationStructure:


ClassificationItemRelationStructure
***********************************


Extends
=======
:ref:`Identifiable`


Definition
==========
A complex RelationStructure for use with statistical classification


Synonyms
========



Explanatory notes
=================
The ClassificationItemRelationStructure has a set of ClassificationItemRelations which are basically adjacency lists. A source ClassificationItem has a described relationship to a target list of ClassificationItem. The semantic might be, for example, "parentOf", or "contains", etc..


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst