.. _ClassificationItem:


ClassificationItem
******************


Extends
=======
:ref:`Designation`


Definition
==========
A Classification Item represents a Category at a certain Level within a Statistical Classification.


Synonyms
========



Explanatory notes
=================
A Classification Item defines the content and the borders of the Category. A Unit can be classified to one and only one item at each Level of a Statistical Classification. As such a Classification Item is a placeholder for a position in a StatisitcalClassification. It contains a Designation, for which Code is a kind; a Category; and other things. This differentiates it from Code which is a kind of Designation, in particular it is an alphanumeric string assigned to stand in place of a category. For example, the letter M might stand for the category Male in the CodeList called Gender.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst