.. _examples:


MeasureRole - Examples
**********************


A data record with four variables: "PersonId", "Systolic", "Diastolic", "Seated" might have a Viewpoint with PersonId defined as having the IdentifierRole Systolic defined as having the MeasureRole Diastolic defined as having the MeasureRole Seated defined as having the AttributeRole PersonId, Systolic, Diastolic, Seated 123,122,20,yes 145,130,90,no