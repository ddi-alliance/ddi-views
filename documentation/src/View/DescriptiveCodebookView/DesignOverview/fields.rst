.. _fields:


DesignOverview - Properties and Relationships
*********************************************





Properties
==========

==============  =================================  ===========
Name            Type                               Cardinality
==============  =================================  ===========
overview        InternationalStructuredString      0..1
subectOfDesign  ExternalControlledVocabularyEntry  0..1
==============  =================================  ===========


overview
########
Short natural language account of the information obtained from the combination of properties and relationships associated with an object. Supports the use of multiple languages and structured text.


subectOfDesign
##############
A term describing the subject of the design. Supports the use of an external controlled vocabulary




Relationships
=============

===================  =================  ===========  ================
Name                 Type               Cardinality  allways external
===================  =================  ===========  ================
assumesPrecondition  Precondition       0..n           yes
expressesAlgorithm   AlgorithmOverview  0..n           no
implementedBy        WorkflowProcess    0..n           yes
isDescribedIn        ExternalMaterial   0..n           no
realizes             Design             0..n           yes
specifiesGoal        Goal               0..n           yes
===================  =================  ===========  ================


assumesPrecondition
###################
The description of a Precondition that must exist prior to the use of this design




expressesAlgorithm
##################
The algorithm that is expressed by the design. Constraint of the pattern relationship expressesAlgorithm by limiting the target to a DescribedAlgorithm.




implementedBy
#############
The process used to implement the Design. WorkflowProcess is a generic realization of the ProcessPattern.




isDescribedIn
#############
An external document or other source that provides descriptive information on the object.




realizes
########
Realization of the Design pattern class




specifiesGoal
#############
A description of the goal of the design. The goal may be used to evaluate the design, or implementations of the design, in terms of its ability to meet the goal.



