.. _fields:


StudySeries - Properties and Relationships
******************************************





Properties
==========

=========  =============================  ===========
Name       Type                           Cardinality
=========  =============================  ===========
contains   StudyIndicator                 0..n
isOrdered  Boolean                        0..1
name       ObjectName                     0..n
overview   InternationalStructuredString  0..1
purpose    InternationalStructuredString  0..1
type       CollectionType                 0..1
=========  =============================  ===========


contains
########
Allows for the identification of the member and optionally provides an index for the member within an ordered array


isOrdered
#########
If members are ordered set to true, if unordered set to false.


name
####
The name of the series as a whole


overview
########
A general descriptive overview of the series.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text.


type
####
Whether the collection is a bag or a set: a bag is a collection with duplicates allowed, a set is a collection without duplicates.




Relationships
=============

===============  ======================  ===========  ================
Name             Type                    Cardinality  allways external
===============  ======================  ===========  ================
definingConcept  Concept                 0..n           no
isStructuredBy   StudyRelationStructure  0..n           no
realizes         StructuredCollection    0..n           yes
===============  ======================  ===========  ================


definingConcept
###############
The conceptual basis for the collection of members.




isStructuredBy
##############
Description of a complex structure for the Collection. A collection may be structured in more than one way for different uses




realizes
########
A StudySeries can contain multiple Studies, or even StudySeries



