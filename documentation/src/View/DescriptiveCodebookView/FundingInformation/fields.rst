.. _fields:


FundingInformation - Properties and Relationships
*************************************************





Properties
==========

===========  =================================  ===========
Name         Type                               Cardinality
===========  =================================  ===========
funderRole   ExternalControlledVocabularyEntry  0..1
grantNumber  String                             0..n
purpose      InternationalStructuredString      0..1
===========  =================================  ===========


funderRole
##########
Role of the funding organization or individual. Supports the use of a controlled vocabulary.


grantNumber
###########
The identification code of the grant or other monetary award which provided funding for the described object.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured tex




Relationships
=============

=========  =====  ===========  ================
Name       Type   Cardinality  allways external
=========  =====  ===========  ================
hasFunder  Agent  0..n           no
=========  =====  ===========  ================


hasFunder
#########
The source of the funding expressed as an Organization or Individual.



