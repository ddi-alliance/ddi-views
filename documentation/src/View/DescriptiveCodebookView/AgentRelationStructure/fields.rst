.. _fields:


AgentRelationStructure - Properties and Relationships
*****************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
effectiveDates            DateRange                          0..1
hasMemberRelation         AgentRelation                      0..n
hasRelationSpecification  RelationSpecification              0..1
privacy                   ExternalControlledVocabularyEntry  0..1
purpose                   InternationalStructuredString      0..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       0..1
========================  =================================  ===========


effectiveDates
##############
The effective start and end date of the relationship


hasMemberRelation
#################
Agent relations that comprise the relation structure


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an ennumeration list. Use if all relations within this relation structure are of the same specification.


privacy
#######
Define the level of privacy regarding this relationship. Supports the use of a controlled vocabulary.


purpose
#######
Explanation of the intent of creating the relation. Supports the use of multiple languages and structured text.


semantic
########
Provides semantic context for the relation structure


totality
########
Type of relation in terms of totality with respect to an associated collection.




Relationships
=============

========  =================  ===========  ================
Name      Type               Cardinality  allways external
========  =================  ===========  ================
realizes  RelationStructure  0..n           yes
========  =================  ===========  ================


realizes
########
Collection Pattern class realized by this class



