.. _examples:


AgentRelationStructure - Examples
*********************************


An individual employed by an Organization. A unit or project (organization) within another Organization.