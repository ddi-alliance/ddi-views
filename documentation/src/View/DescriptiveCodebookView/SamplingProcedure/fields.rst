.. _fields:


SamplingProcedure - Properties and Relationships
************************************************





Relationships
=============

====================  =================  ===========  ================
Name                  Type               Cardinality  allways external
====================  =================  ===========  ================
componentMethodology  SamplingProcedure  0..n           no
hasDesign             SamplingDesign     0..n           yes
hasProcess            SamplingProcess    0..n           yes
isExpressedBy         SamplingAlgorithm  0..n           yes
====================  =================  ===========  ================


componentMethodology
####################
A Sampling Procedure that is a component part of this Sampling Procedure




hasDesign
#########
Sampling Design used to perform the Sampling Procedure




hasProcess
##########
Sampling Process used to perform the Sampling Procedure




isExpressedBy
#############
Sampling Algorithm that defines the methodology in a generic way



