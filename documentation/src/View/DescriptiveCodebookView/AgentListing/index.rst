.. _AgentListing:


AgentListing
************


Extends
=======
:ref:`Identifiable`


Definition
==========
A listing of Agents of any type. The AgentList may be organized to describe relationships between members using AgentRelationStructure.


Synonyms
========



Explanatory notes
=================
Relationships between agents are fluid and reflect effective dates of the relationship. An agent may have multiple relationships which may be sequencial or concurrent. Relationships may or may not be hierarchical in nature. All Agents are serialized individually and brought into relationships as appropriate.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst