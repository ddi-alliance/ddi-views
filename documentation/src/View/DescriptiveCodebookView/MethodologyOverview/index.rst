.. _MethodologyOverview:


MethodologyOverview
*******************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
High level, descriptive, human informative methodology statement used to describe the overall methodology, identify related design, algorithm, and process information. A methodology is normally informed by earlier research and clarifies how earlier research methods were incorporated into the current work.


Synonyms
========



Explanatory notes
=================
This would most commonly be used in a Codebook along with an AlgorithmOverview and a DesignOverview. Note that Process may be described in more detail than a high level overview. Note that the algorithm may be implemented by multiple processes which are not limited to any specific type of Process. This can be constrained by the inclusion of only specific realizations of Process within a Functional View. Note that this MethodologyOverview can be used as a collective description of specific methodologies used by a Study or other broad set of metadata.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst