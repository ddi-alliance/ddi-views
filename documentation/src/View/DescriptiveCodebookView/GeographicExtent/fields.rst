.. _fields:


GeographicExtent - Properties and Relationships
***********************************************





Properties
==========

================  ============  ===========
Name              Type          Cardinality
================  ============  ===========
boundingPolygon   Polygon       0..n
excludingPolygon  Polygon       0..n
geographicTime    DateRange     0..1
hasAreaCoverage   AreaCoverage  0..n
hasCentroid       SpatialPoint  0..1
isSpatialLine     SpatialLine   0..1
locationPoint     SpatialPoint  0..1
================  ============  ===========


boundingPolygon
###############
A description of the boundaries of the polygon either in-line or by a reference to an external file containing the boundaries. Repeatable to describe non-contiguous areas such as islands or Native American Reservations in some parts of the United States.


excludingPolygon
################
A description of a the boundaries of a polygon internal to the bounding polygon which should be excluded. For example, for the bounding polygon describing the State of Brandenburg in Germany, the Excluding Polygon would describe the boundary of Berlin, creating hole within Brandenburg which is occupied by Berlin.


geographicTime
##############
A time for which the polygon is an accurate description of the area. This may be a range (without an end date if currently still valid) or a single date when the shape was know to be valid if a range is not available.


hasAreaCoverage
###############
Means of describing the area covered by the geographic extent either in its entirety (total area) or specific subsets (land, water, urban, rural, etc.) providing a definition and measurement.


hasCentroid
###########
Identifies the centroid of a polygon as a specific point


isSpatialLine
#############
The geographic extent is a single line


locationPoint
#############
The geographic extent is a single point.




Relationships
=============

==============  ===========  ===========  ================
Name            Type         Cardinality  allways external
==============  ===========  ===========  ================
hasBoundingBox  BoundingBox  0..n           no
==============  ===========  ===========  ================


hasBoundingBox
##############
A Bounding Box (North, South Latitude and East, West Longitude) for the geographic extent.



