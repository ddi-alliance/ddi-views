.. _fields:


Access - Properties and Relationships
*************************************





Properties
==========

========================  =============================  ===========
Name                      Type                           Cardinality
========================  =============================  ===========
accessConditions          InternationalStructuredString  0..1
accessPermission          Form                           0..n
citationRequirement       InternationalStructuredString  0..1
confidentialityStatement  InternationalStructuredString  0..1
contactAgent              AgentAssociation               0..n
depositRequirement        InternationalStructuredString  0..1
disclaimer                InternationalStructuredString  0..1
purpose                   InternationalStructuredString  0..1
restrictions              InternationalStructuredString  0..1
validDates                DateRange                      0..1
========================  =============================  ===========


accessConditions
################
A statement regarding conditions for access. May be expressed in multiple languages and supports the use of structured content.


accessPermission
################
A link to a form used to provide access to the data or metadata including a statement of the purpose of the form.


citationRequirement
###################
A statement regarding the citation requirement. May be expressed in multiple languages and supports the use of structured content.


confidentialityStatement
########################
A statement regarding the confidentiality of the related data or metadata.


contactAgent
############
The agent to contact regarding access including the role of the agent.


depositRequirement
##################
A statement regarding depositor requirements. May be expressed in multiple languages and supports the use of structured content.


disclaimer
##########
A disclaimer regarding the liability of the data producers or providers. May be expressed in multiple languages and supports the use of structured content.


purpose
#######
Explanation of the intent of some decision or object. Supports the use of multiple languages and structured text


restrictions
############
A statement regarding restrictions to access. May be expressed in multiple languages and supports the use of structured content.


validDates
##########
The date range or start date of the access description.

