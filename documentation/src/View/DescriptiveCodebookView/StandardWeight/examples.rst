.. _examples:


StandardWeight - Examples
*************************


A simple sample survey without an individual weight variable will have a StandardWeight to be used on all cases: 1% sample has a sample weight of 100