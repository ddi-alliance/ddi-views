.. _StandardWeight:


StandardWeight
**************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
Provides an identified value for a standard weight expressed as an xs:float. This object may be referenced by a variable or statistic and used as a weight for analysis.


Synonyms
========



Explanatory notes
=================
A simple random sample survey without an individual weight variable will have a StandardWeight to be used on all cases. This is common when the sample is a simple random sample of a population with no oversampling to provide larger case counts for small populations.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst