.. _fields:


InstanceVariableRelationStructure - Properties and Relationships
****************************************************************





Properties
==========

========================  =================================  ===========
Name                      Type                               Cardinality
========================  =================================  ===========
criteria                  InternationalStructuredString      0..1
displayLabel              LabelForDisplay                    0..n
hasMemberRelation         InstanceVariableRelation           0..n
hasRelationSpecification  RelationSpecification              1..1
semantic                  ExternalControlledVocabularyEntry  0..1
totality                  TotalityType                       1..1
usage                     InternationalStructuredString      0..1
========================  =================================  ===========


criteria
########
Intensional definition of the order criteria (e.g. alphabetical, numerical, increasing, decreasing, etc.)


displayLabel
############
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.


hasMemberRelation
#################
Member relations of InstanceVariables that comprise the relation structure


hasRelationSpecification
########################
Provides information on reflexivity, transitivity, and symmetry of relationship using a descriptive term from an enumeration list.  Use if all relations within this relation structure are of the same specification.


semantic
########
Controlled vocabulary for the order relation semantics. It should contain, at least, the following: Self_Or_Descendant_Of, Part_Of, Less_Than_Or_Equal_To, Subtype_Of, Subclass_Of.


totality
########
Controlled Vocabulary to specify whether the relation is total, partial or unknown.


usage
#####
A display label for the OrderRelation. May be expressed in multiple languages. Repeat for labels with different content, for example, labels with differing length limitations.




Relationships
=============

========  =================  ===========  ================
Name      Type               Cardinality  allways external
========  =================  ===========  ================
realizes  RelationStructure  0..n           yes
========  =================  ===========  ================


realizes
########
Class of the Collection pattern realized by this class.



