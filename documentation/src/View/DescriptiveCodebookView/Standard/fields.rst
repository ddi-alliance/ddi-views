.. _fields:


Standard - Properties and Relationships
***************************************





Relationships
=============

============  ===================  ===========  ================
Name          Type                 Cardinality  allways external
============  ===================  ===========  ================
compliance    ComplianceStatement  0..n           no
standardUsed  ExternalMaterial     0..n           no
============  ===================  ===========  ================


compliance
##########
Allows for a quality statement based on frameworks to be described using itemized properties. A reference to a concept, a coded value, or both can be used to specify the property from the standard framework identified in StandardUsed. Usage can provide further details or a general description of compliance with a standard.




standardUsed
############
Provide the citation and location of the published standard using the OtherMaterialType.



