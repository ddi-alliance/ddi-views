.. _AttributeRole:


AttributeRole
*************


Extends
=======
:ref:`ViewpointRole`


Definition
==========
An AttributeRole identifies one or more InstanceVariables as being attributes within a ViewPoint. An AttributeRole is a SimpleCollection of InstanceVariables acting in the AttributeRole.


Synonyms
========



Explanatory notes
=================
See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint. An InstanceVariable with an AttributeRole assigned might contain data about the conditions under which the MeausreRole InstanceVariables were collected or other paradata.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst