.. _examples:


GeographicUnit - Examples
*************************


The State of Minnesota from 1858 to date where the Unit Type is a State as defined by the United States Census Bureau. It has a geographic extent and supersedes a portion of the Territory of Minnesota and a portion of the Territory of Wisconsin. Minnesota territory had both a broader geographic extent, different Unit Type, and earlier time period (1849-1858) as did the Territory of Wisconsin.