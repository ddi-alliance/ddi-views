.. _fields:


Code - Properties and Relationships
***********************************





Properties
==========

==============  ===========  ===========
Name            Type         Cardinality
==============  ===========  ===========
representation  ValueString  1..1
==============  ===========  ===========


representation
##############
The value as expressed in the data file. 




Relationships
=============

=======  ========  ===========  ================
Name     Type      Cardinality  allways external
=======  ========  ===========  ================
denotes  Category  0..n           no
=======  ========  ===========  ================


denotes
#######
A definition for the code. Specialization of denotes for Categories.



