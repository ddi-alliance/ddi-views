.. _fields:


VariableStatistics - Properties and Relationships
*************************************************





Properties
==========

====================  =================  ===========
Name                  Type               Cardinality
====================  =================  ===========
hasCategoryStatistic  CategoryStatistic  0..n
hasSummaryStatistic   SummaryStatistic   0..n
totalResponses        Integer            0..1
====================  =================  ===========


hasCategoryStatistic
####################
Statistics that provides a category specific values, filtered or unfiltered, weighted or unweighted, expressed as an xs:decimal or xs:double


hasSummaryStatistic
###################
Statistic that provides a summary value, filtered or unfiltered, weighted or unweighted, expressed as an xs:decimal or xs:double


totalResponses
##############
The total number of responses to this variable. This element is especially useful if the number of responses does not match added case counts. It may also be used to sum the frequencies for variable categories.




Relationships
=============

========================  ================  ===========  ================
Name                      Type              Cardinality  allways external
========================  ================  ===========  ================
applicableWeightVariable  InstanceVariable  0..n           no
forInstanceVariable       InstanceVariable  0..n           no
hasFilterVariable         InstanceVariable  0..n           no
realizes                  CollectionMember  0..n           yes
usesStandardWeight        StandardWeight    0..n           no
========================  ================  ===========  ================


applicableWeightVariable
########################
Reference to a variable to use for weight in calculating the statistic.




forInstanceVariable
###################
Reference to the variable to which the statistics apply.




hasFilterVariable
#################
A variable that may be used to filter the summary and/or category statistics (for example by country or by gender)




realizes
########
Uses the pattern of the abstract class Member




usesStandardWeight
##################
Reference to the StandardWeight value provided in Weighting.



