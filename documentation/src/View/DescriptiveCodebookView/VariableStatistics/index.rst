.. _VariableStatistics:


VariableStatistics
******************


Extends
=======
:ref:`Identifiable`


Definition
==========
Contains summary and category level statistics for the referenced variable.


Synonyms
========



Explanatory notes
=================
Includes information on the total number of responses, the weights in calculating the statistics, variable level summary statistics, and category statistics. The category statistics may be provided as unfiltered values or filtered through a single variable. For example the category statistics for Sex filtered by the variable Country for a multi-national data file. Note that if no weighting factor is identified, all of the statistics provided are unweighted.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst