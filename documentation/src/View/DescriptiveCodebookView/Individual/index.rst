.. _Individual:


Individual
**********


Extends
=======
:ref:`Agent`


Definition
==========
A person who may have a relationship to another Agent and who may be specified as being associated with an act.


Synonyms
========



Explanatory notes
=================
A set of information describing an individual and means of unique identification and/or contact. Information may be repeated and provided effective date ranges to retain a history of the individual within a metadata record. Actions and relationships are specified by the use of the Individual as the target of a relationship (Creator) or within a collection of Agents in an AgentListing (employee of an Organization).


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst