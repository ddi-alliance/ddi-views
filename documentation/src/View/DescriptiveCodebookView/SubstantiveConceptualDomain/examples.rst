.. _examples:


SubstantiveConceptualDomain - Examples
**************************************


An enumeration of concepts for a categorical variable like "male" and "female" for gender.