************************
ControlledVocabularyView
************************
Purpose:

The ControlledVocabulary view is intended to allow for the structured description of a vocabulary in DDI4. A vocabulary can be described as a simple set of terms and definitions, or a set of terms and definitions with a structure such as a hierarchy.

Use Cases:

A DDI4 ExternalControlledVocabularyEntry could reference a vocabulary entry described in DDI4. This would allow processing by software able to interpret DDI4.

DDI4 has a set of controlled vocabularies https://www.ddialliance.org/controlled-vocabularies, currently described in Genericode. These could be published in DDI4 as well.

Vocabularies having a structure more complex than a simple table.


Target Audiences:

DDI4 user communities

Restricted Classes:

Concept is extended by Category, ConceptualVariable, RepresentedVariable, InstanceVariable, Population, UnitType and Universe

ExternalMaterial is extended by ExternalAid

General Documentation:

The representation of a controlled vocabulary in DDI4 consists of four classes:

ControlledVocabulary – The structured collection of vocabulary entries

VocabularyEntry – Assigns a definition to a term

VocabularyRelationStructure – Allows for the description of a complex structure among the terms, such as a hierarchy

ExternalMaterial – Points to material outside of a current DDI4 instance.

The ControlledVocabulary uses a set of  VocabularyEntryIndicators to delineate the entries in the vocabulary. These may be ordered with an index. The VocabularyRelationStructure uses a set of VocabularyEntryRelations to describe relationships among the vocabulary entries. A "source" entry might have a relationship to several "target" entries, for example one entry (e.g. Vehicle) might be a more general term for several other terms (e.g. car, bike, skateboard). This relationship may be described with a semantic, a relationSpecification (e.g. GeneralSpecific), and whether the relation is total or not. 




A functional view is a collection of classes in DDI that covers a functional use case.
These are not namespaces.

.. toctree::
   :caption: Classes
   :maxdepth: 2

   ControlledVocabulary/index.rst
   ExternalMaterial/index.rst
   VocabularyEntry/index.rst
   VocabularyRelationStructure/index.rst



Graph
=====

.. graphviz:: /images/graph/ControlledVocabularyView/ControlledVocabularyView.dot