.. _InstanceVariableRelationStructure:


InstanceVariableRelationStructure
*********************************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A realization of RelationStructure that is used to describe the order of InstanceVariables in a record for those cases where the variables have a more complicated logical order than a simple sequence.


Synonyms
========
An information model, a struct in C, a nested object in JSON


Explanatory notes
=================
For a simple sequence the order can be defined by the index values of the LogicalRecord's InstanceVariableIndicators that a UnitDataRecord inherits. Alternatively, the InstanceVariableRelationStructure can be used by a UnitDataRecord to describe a StructuredCollection as needed.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst