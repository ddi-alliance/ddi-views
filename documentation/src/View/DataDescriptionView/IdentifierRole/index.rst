.. _IdentifierRole:


IdentifierRole
**************


Extends
=======
:ref:`ViewpointRole`


Definition
==========
An IdentifierRole identifies one or more InstanceVariables as being identifiers within a ViewPoint. An IdentifierRole is a SimpleCollection of InstanceVariables acting in the IdentifierRole.


Synonyms
========



Explanatory notes
=================
See the Viewpoint documentation for an in depth discussion of the uses of ViewpointRoles: http://lion.ddialliance.org/ddiobjects/viewpoint


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst