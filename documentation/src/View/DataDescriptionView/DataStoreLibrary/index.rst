.. _DataStoreLibrary:


DataStoreLibrary
****************


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A DataStoreLibrary is a collection or, again, a "library" of DataStores. An individual DataStore is associated with a Study. A collection of DataStores is associated with a StudySeries. The relationships among the DataStores in the DataStoreLibrary is described by the DataStoreRelationStructure. Relations may be more or less complicated depending on the StudySeries type. A StudySeries may be ad hoc. A StudySeries may form a time series. The variety of these collections has been described using the <a href=https://www.ddialliance.org/Specification/DDI-Lifecycle/3.1/XMLSchema/FieldLevelDocumentation/">"Group"</a> in DDI 3.1. Like any RelationStructure, the DataStoreRelationStructure is able to describe both part/whole relations and generalization/specialization relations. See the controlled vocabulary at <a href="http://lion.ddialliance.org/datatypes/relationspecification">RelationSpecification</a> to review all the types of relations a RelationStructure is able to describe.


Synonyms
========
Structured metadata archive


Explanatory notes
=================
If we broke a study down into sub-studies where the core is a sub-study and each supplement is a sub-study, we could also use the DataStoreRelationStructure to track the generalization/specialization relationship or, again, "going deep" over time. Sometimes across a StudySeries we add panels. If panels were also sub-studies, we could use the DataStoreRelationStructure to track both the core and supplements by panel over time.


.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst