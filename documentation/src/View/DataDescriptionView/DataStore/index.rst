.. _DataStore:


DataStore
*********


Extends
=======
:ref:`AnnotatedIdentifiable`


Definition
==========
A DataStore is either a SimpleCollection or a StructuredCollection of LogicalRecords, keeping in mind that a LogicalRecords is a definition, not a "datasets". LogicalRecords organized in a StructuredCollection is called a LogicalRecordRelationStructure. Instances of LogicalRecords instantiated as organizations of DataPoints hosting data are described in FormatDescription. A DataStore is reusable across studies. Each Study has at most one DataStore.


Synonyms
========
Schema repository, data network


Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst