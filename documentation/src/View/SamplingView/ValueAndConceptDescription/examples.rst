.. _examples:


ValueAndConceptDescription - Examples
*************************************


Example 1) The integers between 1 and 10 inclusive. The values of x satisfying the logicalExpression property: " (1 <=x <= 10) AND mod(x,10)=0" Also described with minimumValueInclusive = 1 and maximumValueInclusive = 10 (and datatype of integer) Example 2) The upper case letters A through C and X described with the regularExpression "/[A-CX]/" Example 3) A date-time described with the Unicode Locale Data Markup Language pattern yyyy.MM.dd G 'at' HH:mm:ss zzz