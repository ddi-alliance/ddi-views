.. _fields:


Goal - Properties and Relationships
***********************************





Relationships
=============

=============  ================  ===========  ================
Name           Type              Cardinality  allways external
=============  ================  ===========  ================
isDiscussedIn  ExternalMaterial  0..n           no
=============  ================  ===========  ================


isDiscussedIn
#############
Identifies material discussing the goal. The material may be in DDI or other format.



