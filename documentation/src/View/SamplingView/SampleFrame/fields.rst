.. _fields:


SampleFrame - Properties and Relationships
******************************************





Properties
==========

================  =============================  ===========
Name              Type                           Cardinality
================  =============================  ===========
displayLabel      LabelForDisplay                0..n
limitations       InternationalStructuredString  0..1
name              ObjectName                     0..n
purpose           InternationalStructuredString  0..1
referencePeriod   DateRange                      0..1
updateProcedures  InternationalStructuredString  0..1
usage             InternationalStructuredString  0..1
validPeriod       DateRange                      0..1
================  =============================  ===========


displayLabel
############
A structured display label providing a fully human readable display for the identification of the object. Supports the use of multiple languages and structured text.


limitations
###########
An explanation of any limitations of the sample frame. Supports the use of multiple languages and structured text.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage.


purpose
#######
An explanation of the intent of the sample frame. Supports the use of multiple languages and structured text.


referencePeriod
###############
The reference period for the sample frame. 


updateProcedures
################
A description of how the sample frame is updated and maintained. Supports the use of multiple languages and structured text.


usage
#####
Explanation of the ways in which the sample frame is to be employed. Supports use of multiple languages and structured text.


validPeriod
###########
The period for which the current version is valid




Relationships
=============

====================  ===========  ===========  ================
Name                  Type         Cardinality  allways external
====================  ===========  ===========  ================
hasAccess             Access       0..n           no
hasCustodian          Agent        0..n           no
hasInputParameter     Parameter    0..1           no
hasOutputParameter    Parameter    0..1           no
hasPopulation         Population   0..n           no
hasPrimaryUnitType    UnitType     0..n           no
hasSecondaryUnitType  UnitType     0..n           no
sourceFrame           SampleFrame  0..n           no
====================  ===========  ===========  ================


hasAccess
#########
Accessibility of the sample frame including means of accessing




hasCustodian
############
The agent who is primarily responsible for the maintenance of the sample frame




hasInputParameter
#################
SampleFrame can serve as an input to a sampling process.




hasOutputParameter
##################
SampleFrame can be an output related to a sampling process.




hasPopulation
#############
The population included in the sample frame




hasPrimaryUnitType
##################
The primary unit type of the population




hasSecondaryUnitType
####################
A secondary unit type identifiable with the population




sourceFrame
###########
A sample frame which served as a source for the sample frame being described



