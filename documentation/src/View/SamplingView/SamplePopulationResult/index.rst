.. _SamplePopulationResult:


SamplePopulationResult
**********************


Extends
=======
:ref:`Result`


Definition
==========
The Sample Population is the result of a sampling process. It is the subset of the population selected from the Sample Frame or Frames that will be used as respondents to represent the target population. Sample Populations may occur at a number of stages in the sampling process and serve as input to a subsequent sampling stage. The Sample Population should be validated against the intended sample population as described in the goal of the design and/or process.


Synonyms
========



Explanatory notes
=================



.. toctree::
   :caption: Classes
   :maxdepth: 2

   examples.rst
   fields.rst
   ddi32mapping.rst
   rdfmapping.rst
   gsimmapping.rst
   graph.rst