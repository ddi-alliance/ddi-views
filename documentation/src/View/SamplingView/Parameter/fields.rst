.. _fields:


Parameter - Properties and Relationships
****************************************





Properties
==========

===============  =============================  ===========
Name             Type                           Cardinality
===============  =============================  ===========
agency           String                         1..1
alias            UnlimitedNatural               0..1
defaultValue     ValueString                    0..1
id               String                         1..1
isArray          Boolean                        0..1
limitArrayIndex  UnlimitedNatural               0..1
name             ObjectName                     0..n
purpose          InternationalStructuredString  0..1
version          String                         1..1
===============  =============================  ===========


agency
######
This is the registered agency code with optional sub-agencies separated by dots. For example, diw.soep, ucl.qss, abs.essg.


alias
#####
If the content of the parameter is being used in a generic set of code as an alias for the value of an object in a formula (for example source code for a statistical program) enter that name here. This provides a link from the identified parameter to the alias in the code.


defaultValue
############
Provides a default value for the parameter if there is no value provided by the object it is bound to or the process that was intended to produce a value.


id
##
The ID of the object. This must conform to the allowed structure of the DDI Identifier and must be unique within the Agency.


isArray
#######
If set to "true" indicates that the content of the parameter is a delimited array rather than a single object and should be processed as such.limitArrayIndex


limitArrayIndex
###############
When the Parameter represents an array of items, this attribute specifies the index identification of the items within the zero-based array which should be treated as that associated with the parameter. If not specified, the full array is treated as the parameter.


name
####
A linguistic signifier. Human understandable name (word, phrase, or mnemonic) that reflects the ISO/IEC 11179-5 naming principles. If more than one name is provided provide a context to differentiate usage. 


purpose
#######
Explanation of the intent of the Agent Listing. Supports the use of multiple languages and structured text.


version
#######
The version number of the object. The version number is incremented whenever the non-administrative metadata contained by the object changes.




Relationships
=============

===================  ===========  ===========  ================
Name                 Type         Cardinality  allways external
===================  ===========  ===========  ================
valueRepresentation  ValueDomain  0..n           no
===================  ===========  ===========  ================


valueRepresentation
###################
If the content of the parameter contains representational content, such as codes, provide the representation here. ValueDomain is the abstract with a number of subtype and may be replaced with any valid substitution for ValueDomain. Inclusion of the valueRepresentation is recommended if you will be sharing data with others as it provides information on the structure of what they can expect to receive when the parameter is processed.



