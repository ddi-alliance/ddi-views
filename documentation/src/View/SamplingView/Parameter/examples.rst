.. _examples:


Parameter - Examples
********************


A question might have dynamic text or, again, "fills". Depending on the gender of the subject, a question might say "he" or "she" and/or "him" or "her". A gender variable is passed into the question, and question code resolves the dynamic text. A computation action may be expecting a numeric array of values [result of looping an "Age in years" question through every member of a household]. isArray = "true"; valueRepresentation would link to a SubstantiveValueDomain defining a numeric value with a possible valid range; and because the parameter occurs in the context of a computation action it is necessary to specify an alias (e.g. AGE). The alias links the parameter to the code in the computation.