Classification Family: Industry
	NAICS
	SIC
	
Classification Series: NAICS
	2017
	2012
	2012.1
	2012.2
	Structure: ParentChild
	
		2012
	   _______|_______
	   |		  |
	2012.1		2012.2
			  |
			2017  
	
Classification Series: SIC
	2007
	2014
	2014.1
	2014.2
	Structure: ParentChild
	
		2007
	          |
		2014
	   _______|_______
	   |		  |
	2014.1		2014.2
	
SIC
Level	value
0	0
1	23
2	236
2	237
1	51
2	511
2	518

Level
0 = Industry
1 = Sector
2 = Subsector

	Structure: WholePart
		  0
	   _______|_______
	   |		  |
	23		  51

	Structure: WholePart
		  23
	   _______|_______
	   |		  |
	  236		 237

	Structure: WholePart
	 	  51
	   _______|_______
	   |		  |
	  511		 518
	